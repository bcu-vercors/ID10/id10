============
ID10 project
============

[![build status](https://gitlab.esrf.fr/ID10/id10/badges/master/build.svg)](http://ID10.gitlab-pages.esrf.fr/ID10)
[![coverage report](https://gitlab.esrf.fr/ID10/id10/badges/master/coverage.svg)](http://ID10.gitlab-pages.esrf.fr/id10/htmlcov)

ID10 software & configuration

Latest documentation from master can be found [here](http://ID10.gitlab-pages.esrf.fr/id10)
