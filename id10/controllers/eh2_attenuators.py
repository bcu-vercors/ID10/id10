# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) 2016 Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.


"""
Class attenuator serves to control attenuator with several
filters the insertion/extraction of which is controlled by 
Wago DO (Digital Output) channels and their status by
Wago DI (Digital Input) channels.

The usage of Wago channels on ID10 EH2 is not uniform across all filters.

On ID10 EH2 there is:
- 'old' attenuator with 4 filters, where:
  - Wago 4ch. DO module 750-516 is used such that one DO channel
    controls 1 filter insertion(1)/extraction(0) and
    It is important to note, that the hardware of this 'old'
    attenuator is such that the command bits must be inverted in
    order to achieve desired state i.e. when a DO channel is at 1,
    the relevant filter is extracted and not inserted as one would
    expect!
  - Wago 4ch. DI module 750-403 is used such that one DI channel
    controls 1 filter insertion(1)/extraction(0) status. 
- 2 'new' attenuators with 4 filters each, where:
  - Wago 8ch. DO module 750-530 us used such that one DO channel
    controls 1 filter insertion(1)/extraction(0), but only even 
    channels are used and
  - Wago 8ch. DI module 750-436 is used such that 2 DI channels
    control 1 filter insertion/extraction status. The even channels
    indicate IN state and odd channels indicate OUT state. 2 channels
    together are tested and state (1,0) means the filter is IN and
    (0,1) means the filter is OUT. 
    Since the hardware related to DI channels on ID10 EH2 was not
    in good shape, we initially relied on the situation as set in 
    control channels. Now (July 2018) status channels seem to work,
    but are cabled in the reversed way. This inversion is taken into
    account by stat_reverse flag in the config (.yml) file.

To indicate that the control channel bit must be inversed, there
is a flag ctrl_inv in the config file.
ctrl_inv: True ... if need to inverse control bits for desired effect
		   (this must be used for 'old' ID10 EH2 attenuator)
	  False ... if control bits do not need to be inverted 
		   (this must be used for 2 'new' ID10 EH2 attenuators)

To have possibility to avoid using status channels, there is a 
flag 'stat_used' in the config file.
stat_used: True  ... if can    use status channels
           False ... if cannot use status channels
To have possibility to use status channels even when they are reversed
with the respect to the control channels, there is a flag 'stat_reverse'
in the config file.
stat_reverse: True  ... if status channels are reversed with respect to control channels
              False ... if status channels are in the same order as control channels.

Of course there are other parameters to further identify the
relevant channels. We pass the Wago logical names:
ctrl_id: Wago logical name for control channel (single name 
         that is repeated for all (4) channels)
stat_id: Wago logical name fot status channel(s). When 1 channel
         used per filter, then give single name that is repeated
         across all (4) channels. When 2 channels are used per
         filter then logical names of both are passed (and the
         same pair repeats across all channels)
In the config file there are some more parameters, like:
name ... attenuator name 
wago ... reference to Wago object (defined in the separate .yml file)
nb_filters ... nb of filters in 1 attenuator
bitmask_inv ... how user interprets filters in/out sequence as a bit
                mask. Bitmask is a convenient way to set position 
                of all filters (in or out) with one value and also
                when reading their status to get one value, but on 
                ID10 for ex.  the convention of bits is opposite 
                between 'old' and 'new' attenuators:
                In the 'old' one the 1st filter along the beam 
                has power 0 and the 4th power 3, while for 'new' 
                attenuators the ID10 convention is inversed. 
                So will consider that the bitmask is not inverted 
                (False) for the 'old' attenuator and inverted (True)
                for the 'new' attenuators.  

Example of yml configuration file:

class: attenuator
attenuators:
- name: "attSi80"
  #wago_name: "wcid10i"
  wago: $wcid10i
  nb_filters: 4
  ctrl_id: "feh3b"
  ctrl_inv: True
  stat_id: "fstat3b"
  stat_used: True
  stat_reverse: False 
  bitmask_inv: False
  package: id10.controllers.eh2_attenuators

- name: "attGe75"
  #wago_name: "wcid10i"
  wago: $wcid10i
  nb_filters: 4
  ctrl_id: "feh3ge"
  ctrl_inv: False
  stat_id: ["feh3gein", "feh3geout"]
  #stat_used: False
  stat_used: True 
  stat_reverse: True 
  bitmask_inv: True
  package: id10.controllers.eh2_attenuators

- name: "attSi100"
  #wago_name: "wcid10i"
  wago: $wcid10i
  nb_filters: 4
  ctrl_id: "feh3si"
  ctrl_inv: False
  stat_id: ["feh3siin", "feh3siout"]
  #stat_used: False
  stat_used: True 
  stat_reverse: True 
  bitmask_inv: True
  package: id10.controllers.eh2_attenuators

"""
 
import math
import time
from bliss.controllers import wago


class attenuator():

    # ------ constructor --------
    def __init__(self, name, config):
        self.wago  = config["wago"]
        self.nb_filters = config["nb_filters"]
        self.ctrl_id    = config["ctrl_id"]
        self.ctrl_inv   = config["ctrl_inv"]

        self.stat_id    = config["stat_id"]
        self.stat_used  = config["stat_used"]
        self.stat_reverse  = config["stat_reverse"]

        self.bitmask_inv = config["bitmask_inv"]

        # TODO later:
        #self.material   = config["material"]
        #self.thickness  = config["thickness"]
        #self.thick_unit = config["thick_unit"]

        # Next call and definition of the function 'connect' 
        # are no more needed since the reference to the Wago 
        # object is in the config file of an attenuator.	
        #self.connect()


    #def connect(self):
    #    self.wago = wago.WagoController(self.wago_name)

        
    # ------ exit --------
    def exit(self):
        self.wago.close()


#    def invert_bit_values(self,alist):
#	""" 
#	Auxilliary function which from a list where items have value 
#	0 or 1 makes a new list with inverted values(0->1, 1->0).
#       This one leaves original list intact.
#	"""
#	blist = []
#	for i in range(len(alist)):
#           blist.append(1-alist[i])
#	return blist 


    # ------ invert_bit_values --------
    def invert_bit_values(self,alist):
        """
        Auxilliary function which in a list where items have 
        value 0 or 1 inverses values (0->1, 1->0).
        This one modifies original list
        """
        for i in range(len(alist)):
            alist[i] ^= 1
        return alist 


    # ------ get --------
    def get(self):
        """
        Read the position of all filters belonging to an attenuator.
        If status channels hardware is OK, use status channels,
        otherwise rely on control channels.
        - pos is an array/list of 1 and 0 of the length 'nb_filters'
         1 = filter inserted, 0 = filter extracted 
         [0] ... the first filter on the way of X-ray beam
        [nb_filters-1] ... the last filter on the way ....
        Returns: tuple which contains:
         - overall value and
         - a list/array of status (1=inserted/0=extracted) of 
               individual filters  
        """

        bitmask = 0
        pos = []
 
        if self.stat_used == True:

            # Status channels ARE USABLE (case of 'old' attenuator)
            if type(self.stat_id) is str:
                # 1 status channel is used for 1 control channel
                pos = self._wago_get(self.stat_id)
            else:
                # 2 status channels are used for 1 control channel
                # (even-channels = in, odd-channels = out status)
                inpos = self._wago_get(self.stat_id[0])
                outpos = self._wago_get(self.stat_id[1])
                for i in range(self.nb_filters):
                    pos.append((1-outpos[i]) * inpos[i])
                if self.stat_reverse == True:
                    pos.reverse()

        else:
            # Status channels NOT USABLE --> rely on control channels 
            pos = self._wago_get(self.ctrl_id)
            if self.ctrl_inv == True:
                pos = self.invert_bit_values(pos)


        # Create bitmask from individual filter in/out bits
        # in the list. Bitmask reflects overall value. Because
        # of different convention concerning power of 2 weight
        # of individual filters between old and new attenuators
        # given value does not mean the same in/out filter states.
        # For ex. value 3 with old attenuator means that 1st and
        # 2nd filter along the beam are in, while the same value 
        # for the 2 new filters means that the 3rd and 4th filter
        # are in the beam.
        if self.bitmask_inv == False:
            for i in range(self.nb_filters):
                bitmask = bitmask | (pos[i] << i)
        else:
            for i in range(self.nb_filters):
                bitmask = bitmask | pos[i] << (self.nb_filters-1 - i)
        return (bitmask, pos)

    
    def __info__(self):
        """
        Shows the inserted/extracted (in/out) status of individual
        filters of a given attenuator.
        """

        # Get first current filter status
        (bitmask,pos) = self.get()

        # Fields for 'title/header' line
        begstr = " #  "
        endstr = " <------ X-rays ---"
        lbl = "F"

        order = []
        if self.bitmask_inv == False:
            for i in reversed(range(self.nb_filters)):
                order.append(i)
        else:
            for i in range(self.nb_filters):
                order.append(i)

        # 'Title' line
        mystr = begstr
        for i in order:
            mystr += lbl + str(int(math.pow(2,i))) + "  "
        mystr += endstr

        # 'In/Out' status line
        mystr += "\n%2d" % bitmask
        mystr += "  "
        for i in order:
            if bitmask & (1<<i) == 0:
                mystr += "OUT "
            else:
                mystr += "IN  "
        return mystr


    # ------ set --------
    def set(self, bitmask):
        """
        Sets the filters of a given attenuator in the desired
        inserted/extracted (in/out) state. 
        The value passed is the overall value (not individual filter values)
        """

        maxval = (1 << self.nb_filters) - 1 
        if bitmask < 0 or bitmask > maxval:
            raise RuntimeError("Required value is out of bounds [0,%d]" % maxval)
            return

        # Get current filter status
        (currmask,currpos) = self.get()
        newpos = []
        if self.bitmask_inv == False:
            # for 'old' attenuator
            for i in range(self.nb_filters):
                if bitmask & (1 << i):
                    newpos.append(1)
                else:
                    newpos.append(0)
        else:
            # for 'new' attenuators
            for i in reversed(range(self.nb_filters)):
                if bitmask & (1 << i):
                    newpos.append(1)
                else:
                    newpos.append(0)

        # intermediate position (is OR of current + new wanted)
        interm = []
        for i in range(self.nb_filters):
            temp = newpos[i] | currpos[i]
            if self.ctrl_inv == True:
                temp ^= 1
            interm.append(temp)

        self.wago.set(self.ctrl_id,interm)
        time.sleep(0.5)

        # finally apply new setting
        final = newpos
        if self.ctrl_inv == True:
            final = self.invert_bit_values(newpos)
        self.wago.set(self.ctrl_id,final)
        time.sleep(0.5)


        # Read back for check
        # Get current filter status
        (currmask,currpos) = self.get()

        if self.ctrl_inv == True:
            newpos = self.invert_bit_values(newpos)

        errcnt = 0
        for i in range(self.nb_filters):
            if currpos[i] != newpos[i]:
                errcnt = errcnt + 1
                raise RuntimeError("Filter channel %s[%d] not properly set" % (self.ctrl_id,i))
        if errcnt == 0:
            print("New filters positions OK")


    # ------ setmax --------
    def setmax(self):
        """
        Set maximum possible value for an attenuator (= all filters inserted)
        """
        maxbitmask = (1 << self.nb_filters) - 1
        self.set(maxbitmask)


    # ------ setmin --------
    def setmin(self):
        """
        Set minimum value for an attenuator (= 0 = all filters extracted)
        """
        minbitmask = 0
        self.set(minbitmask)

    def __repr__(self):
        return self.__info__()

    def _wago_get(self, channel):
        return [int(i) for i in self.wago.get(channel)]
