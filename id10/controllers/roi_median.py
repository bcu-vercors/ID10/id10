import numpy as np

from bliss.controllers.counter import CalcCounterController


class MedianCalcCounterController(CalcCounterController):
    def calc_function(self, input_dict):
        med = np.median(np.array([v for v in input_dict.values()]))

        return {self.tags[self.outputs[0].name]: med}
