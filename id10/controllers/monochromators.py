# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) 2015-2020 Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

from bliss.controllers.monochromator.monochromator import Monochromator
from bliss.shell.standard import umv

        
class ID10CCMono(Monochromator):
    def __init__(self, config):
        super().__init__(config)
    
    def _load_config(self):

        super()._load_config()
        """
        Motors
        """
        motors_conf = self.config.get("motors", None)
        for motor_conf in motors_conf:
            if "change_motor" in motor_conf.keys():
                self._motors["change_motor"] = motor_conf.get("change_motor")
 
        self.low_pos = {}
        self.high_pos = {}
        self.offset = {}
        
        self.low_pos = self._xtals.get_xtals_config("low_pos")
        self.high_pos = self._xtals.get_xtals_config("high_pos")
        self.offset = self._xtals.get_xtals_config("bragg_offset")

    def _xtal_is_in(self, xtal):
        current_pos = self._motors["change_motor"].position
        if current_pos > self.low_pos[xtal] and current_pos < self.high_pos[xtal]:
            return True
        return False
        
    def _xtal_change(self, xtal):
        if not self._xtal_is_in(xtal):
            target_pos = self.low_pos[xtal] + (self.high_pos[xtal] - self.low_pos[xtal]) / 2.0
            umv(self._motors["change_motor"],target_pos)
            self._motors["bragg"].position += self.offset[xtal]

    def __info__(self):
        motor = self._motors["change_motor"]
        info_str = super().__info__() + "\n"
        info_str += f"Crystal change motor {motor.name} at {motor.position} {motor.unit}, ranges: \n"
        for xtal in self._xtals.xtal_names:
            info_str += f"  - {xtal} [{self.low_pos[xtal]}, {self.high_pos[xtal]}]\n"
        return info_str
