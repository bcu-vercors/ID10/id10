# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) 2015-2022 Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

"""
Control of Harvard apparatus Syringe Pump Pico Plus/11 Plus  through serial line

Use rconfig%B% to configure syringe parameters
Use __info__ to display current settings
Use start, stop to start/stop infuse
A counter can be used to get the current accumulated volume infused. 
At each start command, this volume is reset to zero.
To close remote connection and enable keyboard interaction, use close.
 
yml configuration example:

- class: SyrPumpHarvard
  plugin: generic
  package: id10.controllers.syrpump_harvard
  name: syrpump
  model: 11plus #or picoplus (default: 11plus)
  address: 1 #from 0 to 99 (default: 1)
  serial:
    url: ser2net://lid102:28000/dev/ttyRP24

  counters:
    - counter_name: syrvolume
"""

import gevent
import click

from bliss import global_map
from bliss import current_session
from bliss.common.utils import autocomplete_property

from bliss.common.counter import SamplingCounter
from bliss.controllers.counter import SamplingCounterController
from bliss.comm import get_comm
from bliss.common.logtools import log_info, log_debug


class SyrPumpHarvard:
    def __init__(self, config):

        self._name = config.get("name")
        self._config = config
        self._model = self._config.get("model", "11plus")
        if self._model not in ["picoplus", "11plus"]:
            raise ValueError("Wrong controller model, try picoplus or 11plus")

        self._address = self._config.get("address", 1)
        if self._address not in range(0, 99):
            raise ValueError("Wrong controller address, try [0-99]")

        # hard set serial parameters
        #comm_options = {"stopbits": 2, "bytesize": 8}
        #self._comm = get_comm(self._config, **comm_options)
        self._comm = get_comm(self._config)

        global_map.register(self, children_list=[self._comm])

        # create counters and axis if required
        self._create_sampling_counters(config)
        self.__status = {
            ":": "Stopped",
            ">": "Running forward",
            "<": "Running backward",
            "*": "Stalled",
        }
        self.__rate_units = {}
        self.__rate_units["picoplus"] = {
            "ML/HR": "MLH",
            "ML/MN": "MLM",
            "UL/HR": "ULH",
            "ML/MN": "MLM",
            "NL/HR": "NLH",
            "NL/MN": "NLM",
            "P/LHR": "PLH",
            "PL/MN": "PLM",
        }
        self.__rate_units["11plus"] = {
            "ML/HR": "MLH",
            "ML/MN": "MLM",
            "UL/HR": "ULH",
            "UL/MN": "ULM",
        }
        self._rate_unit = None
        self.__way = 1
        self._vol_unit = ""

    def __info__(self):
        info_list = []
        info_list.append(
            f"Harvard Apparatus Syringe Pump - Model {self._model} @ addr. {self._address}"
        )

        dia = self.diameter
        rate = self.rate
        target = self.target
        unit = self.rate_unit
        dir = "FORWARD" if self.__way > 0 else "BACKWARD"
        vol = self.volume
        info_list.append(f" - Diameter : {dia} mm")
        info_list.append(f" - Rate     : {rate}  {unit.lower()}")
        info_list.append(f" - Target   : {target} {self._vol_unit}")
        info_list.append(f" - Status   : {self._status}")
        info_list.append(f" - Current Infused Volume : {vol:g} {self._vol_unit}")
        info_list.append(f" - Last direction : {dir}")

        return "\n".join(info_list)

    # ---- BEGIN USER METHODS --------------------------------------------------------------

    @property
    def volume(self):
        """
        Return the infused volume unit is as set with rate_unit
        volume is signed depending if the pump goes in forward or backward direction
        """
        vol = self.get_param("VOL")
        return float(vol) * self.__way

    @property
    def target(self):
        target = self.get_param("TAR")
        return float(target)

    @property
    def rate_unit(self):
        unit = self.get_param("RNG")
        self._vol_unit = unit.split("/")[0].lower()
        # update counter unit
        self._cc.counters[0].unit = self._vol_unit
        return unit.lower()

    @rate_unit.setter
    def rate_unit(self, value):
        units = self.__rate_units[self._model].keys()
        if value.upper() not in units:
            raise ValueError(f"Not supported unit {value}, try {list(units)}")
        self.__rate_unit = value.upper()
        self._vol_unit = value.split("/")[0].lower()
        # update counter unit
        self._cc.counters[0].unit = self._vol_unit

    @property
    def rate(self):
        return float(self.get_param("RAT"))

    @rate.setter
    def rate(self, value):
        if self.__rate_unit is None:
            self.__rate_unit = self.rate_unit
        cmd = self.__rate_units[self._model][self.__rate_unit]
        self.set_param(cmd, value)

    @property
    def diameter(self):
        dia = self.get_param("DIA")
        return float(dia)

    @diameter.setter
    def diameter(self, value):
        dia = float(value)
        self.set_param("MMD", dia)

    def start(self, target_volume, reverse=False):
        """
        Start the pump up to target_volume infused. Units is as set in rate_unit
        """
        # check if the pump is not yet running, any cmd get the status as well
        vol = self.volume
        if self._status != "Stopped" and self._status != "Stalled":
            print(f"Warning: syringe pump is already running, please stop it before !!")
            return
        # clear the infused volume
        self.command("CLV")

        if self._model == "picoplus":
            self.set_param("TGT", target_volume)
        else:
            self.set_param("MLT", target_volume)
        # alway move forward with the start command
        if reverse:
            self.command("REV")
        self.command("RUN")
        # read at least one parameter to get direction updated
        vol = self.volume

    def stop(self):
        """
        stop the pump
        """
        self.command("STP")

    def configure(self, diameter=None, rate=None, unit=None):
        """
        Configure syringe pump parameters:
        diameter : syringe diameter in mm.
        rate : pump flow rate
        unit : rate unit
        """
        if diameter is None:
            diameter = click.prompt("Syringe diameter in mm", default=self.diameter)
        if rate is None:
            rate = click.prompt("Flow rate", default=self.rate)
        if unit is None:
            units = [i.lower() for i in self.__rate_units[self._model].keys()]
            print(f"Rate units: {units}")
            unit = click.prompt("   used unit", default=self.rate_unit)
        self.rate_unit = unit
        self.diameter = diameter
        self.rate = rate

    # ---- END USER METHODS ----------------------------------------------------------------
    def get_param(self, name):
        """
        Return the <name> parameter current value
        """
        return self._write_read(name)

    def set_param(self, name, value):
        """
        Set the <name> parameter to value <value>
        """
        self._write_read(name, value)

    def command(self, name):
        """
        Execute the command <name>
        """
        self._write_read(name)

    def _write_read(self, name, value=None):
        addr = f"{self._address:02d}"
        value = "" if value == None else value
        cmd = f"{addr}{name}{value}\r"

        eol = f"\r\n{addr}"
        raw = self._comm.write_readline(
            cmd.encode(), eol=eol.encode(), timeout=3
        ).decode()
        status = self._comm.read(1).decode()
        lines = raw.split("\r\n")

        if len(lines) == 1:
            ans = lines[0]
        else:
            ans = lines[1]
        if ans == "?":
            raise ValueError(f"{name} is an unrecognized command")
        elif ans == "OOR":
            if value:
                raise ValueError(f"Value out-of-range {value} for parameter {name}")
            else:
                raise ValueError(
                    f"Cannot execute command {name} a parameter is out-of-range"
                )

        self._status = self.__status[status]
        if status == ">":
            self.__way = 1
        elif status == "<":
            self.__way = -1

        return ans

    @property
    def name(self):
        return self._name

    @autocomplete_property
    def counters(self):
        """ Standard counter namespace """

        return self._cc.counters

    def _create_sampling_counters(self, config, export_to_session=True):
        """
        """
        cnts_conf = config.get("counters")
        if cnts_conf is None:
            self._cc = None
            self._calc_counter = None
            return

        # create the sampling counters
        self._cc = SyrPumpCounterController(self.name, self)

        for conf in cnts_conf:
            name = conf["counter_name"].strip()
            mode = conf.get("mode", "SINGLE")
            cnt = self._cc.create_counter(SamplingCounter, name, mode=mode)
            if export_to_session:
                if current_session is not None:
                    if (
                        name in current_session.config.names_list
                        or name in current_session.env_dict.keys()
                    ):
                        raise ValueError(
                            f"Cannot export object to session with the name '{name}', name is already taken! "
                        )

                    current_session.env_dict[name] = cnt


class SyrPumpCounterController(SamplingCounterController):
    def __init__(self, name, syrpump):
        super().__init__(name)
        self.syrpump = syrpump

    def read_all(self, *counters):
        values = []
        for cnt in counters:
            values.append(self.syrpump.volume)
        return values

    def read(self, counter):
        return self.syrpump.volume
