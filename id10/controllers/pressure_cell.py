# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) 2015-2020 Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

"""
This class is the main controller of PressureCell setup.
Control of the setup is done via an eurotherm 3500 (modbus TCP).
Pressure Cell is designed and maintained by Karim Lhoste

yml configuration example:

- class: PressureCell
  plugin: bliss
  package: id10.controllers.pressure_cell
  controller_ip: 160.103.30.184
  name: presscell

"""

import enum
import gevent
import time
from functools import partial

from bliss import global_map
from bliss import current_session
from bliss.common.utils import autocomplete_property

from bliss.common.counter import SamplingCounter
from bliss.controllers.counter import SamplingCounterController

from bliss.common.logtools import log_info, log_debug
from bliss.common.soft_axis import SoftAxis
from bliss.common.axis import AxisState

from bliss.common.protocols import IterableNamespace

from bliss.comm.modbus import ModbusTcp

name2address = dict(
    [ # name, address, rwmode, low, high, conv-factor, descr, unit
        ("AM",(0x0111, 3, 0, 1, "AutoManual switch","n/a")),
        ("PV",(0x0001, 1, -9999999999, 9999999999, "Process Value", "Bar")),
        ("TSP",	(0x0002, 3, -9999999999, 9999999999, "Target Set Point", "Bar")),
        ("WSP",	(0x0005, 1, -9999999999, 9999999999, "Working Set Point", "Bar")),
        ("PID_D", (0x0009, 3, 0, 9999999999, "Loop 1 - Derivative time", "n/a")),
        ("PID_D2", (0x0033, 3, 0, 99999, "Loop 1 - Derivative time 2", "n/a")),
        ("PID_I", (0x0008, 3, 0, 9999999999, "Loop 1 - Intergral Time", "n/a")),
        ("PID_I2", (0x0031, 3, 0, 99999, "Loop 1 - Intergral Time 2", "n/a")),
        ("PID_P", (0x0006, 3, 0, 99999, "Loop 1 - Proportinal Band", "n/a")),
        ("PID_P2", (0x0030, 3, 0, 9999999999, "Loop 1 - Proportinal Band 2", "n/a")),
        ("SPR",	(0x0023, 3, 0, 9999999999, "Set Point Rate", "n/a")),
        ("SPRD", (0x004E, 3, 0, 1, "Set Point Rate Disable" ,"n/a")),
        ("PVM1", (0x016C, 1, -9999999999, 9999999999, "Process Value Mod.1A" ,"n/a")),
        ("PVM2", (0x016F, 1, -9999999999, 9999999999, "Process Value Mod.2A" ,"n/a")),
        ("PVM3", (0x0122, 1, -9999999999, 9999999999, "Process Value Mod.3A" ,"n/a")),
        ("PVM4", (0x0175, 1, -9999999999, 9999999999, "Process Value Mod.4A" ,"n/a")),
        ("PVM5", (0x0178, 1, -9999999999, 9999999999, "Process Value Mod.5A","n/a" )),
        ("PVM6", (0x017B, 1, -9999999999, 9999999999, "Process Value Mod.6A" ,"n/a")),
    ]
)


class PressureCell(object):

    def __init__ (self, name, config):

        self._mode = {1:'OFF',0: 'ON'}
            
        self._name = name
        self.config = config
        
        # create the ModbusTCP connection, set identifier to 1, default 255 is not correct
        self._ip = config.get("controller_ip")
        self._modbus = ModbusTcp(self._ip, unit=1)
        
        global_map.register(self, children_list=[self._modbus])

        # create counters and axis if required
        self._create_sampling_counters(config)
        self._create_soft_axes(config)
        

    def __info__(self):
        info_list = []
        info_list.append(f"ID10 Pressure Cell - IP addr. = {self._ip}")

        state = self.read('AM')
        pv = self.read('PV')/100
        wsp = self.read('WSP')/100
        tsp = self.read('TSP')/100
        info_list.append(f" - Regulation is {self._mode.get(state)}")
        info_list.append(f" - Process Value     = {pv}  bar")
        info_list.append(f" - Target Setp Point = {tsp} bar")
        info_list.append(f" - Working Set Point = {wsp} bar")
        return "\n".join(info_list)
        
    def _get_address(self, name):
        name_key = name.upper()
        try:
            address = name2address[name_key][0]
        except KeyError:
            raise RuntimeError(f"{name} does not exist in address mapping")
        else:
            return address

    def _get_all(self, name):
        name_key = name.upper()
        try:
            address = name2address[name_key][0]
        except KeyError:
            raise RuntimeError(f"{name} does not exist in address mapping")
        else:
            return name2address[name_key]
        
        

    # ---- BEGIN USER METHODS --------------------------------------------------------------
    
    def read(self, name):
        address = self._get_address(name)
        return self._modbus.read_holding_registers(address, "h")

    def write(self, name, value):
        address = self._get_address(name)
        self._modbus.write_register(address, "h", value)

    def start(self):
        """
        start the regulation
        """
        self.write('am',0)

    def stop(self):
        """
        start the regulation
        """
        self.write('am',1)
        

    # ---- END USER METHODS ----------------------------------------------------------------
    @property
    def name(self):
        return self._name
    
    @autocomplete_property
    def axes(self):
        return IterableNamespace(**{v.name: v for v in self._soft_axes.values()})

    @autocomplete_property
    def counters(self):
        """ Standard counter namespace """

        return self._cc.counters
    
    def _create_soft_axes(self, config):

        self._axes_tolerance = {}
        self._soft_axes = {}
        self._moving = False
        
        axes_conf = config.get('axes', [])

        for conf in axes_conf:
            name = conf['axis_name'].strip()
            chan = conf['channel'].strip().upper()
            # We only only support soft axis for Target Set Point
            if chan not in name2address.keys() and chan != 'TSP':
                raise RuntimeError(f" channel {chan} is not supported by PressureCell controller")
            address, mode, low, high, descr, _ = self._get_all(chan)
            if mode != 3:
                raise RuntimeError(f" channel {chan} is read-only and cannot be used as soft axis")
            
            low_limit = conf.get("low_limit")
            if low_limit is None or low_limit < low: low_limit = low
            high_limit = conf.get("high_limit")
            if high_limit is None or high_limit > high: high_limit = high
            
            tol = conf.get("tolerance", 0)
            unit = conf.get("unit")
            self._axes_tolerance[chan] = float(tol)
            time = conf.get("time",0)
            self._time = time
            
            self._soft_axes[chan] = SoftAxis (
                name,
                self,
                position=partial(self._axis_position, channel=chan),
                move=partial(self._axis_move, channel=chan),
                stop=partial(self._axis_stop, channel=chan),
                state=partial(self._axis_state, channel=chan),
                low_limit=low_limit,
                high_limit=high_limit,
                tolerance=self._axes_tolerance[chan],
                unit=unit,
            )

    def _axis_position(self, channel=None):
        """ Return the actual value of the given channel """
        if channel.lower() == 'tsp':
            tsp = self.read(channel)/100
            return tsp
        return self.read(channel)

    def _axis_move(self, pos, channel=None):
        """ Set the setpoint to a new value as the target position of the associated soft axis"""
        if channel.lower() == 'tsp':
            val = int(pos*100)
            self.write(channel, val)
            am = self.read('AM')
            self._sp = pos
            self._inside = 0
            self._moving = True if am == 0 else False
        else:
            self.write(channel, pos)

    def _axis_stop(self, channel=None):
        """ Stop the motion of the associated soft axis """
        pass

    def _axis_state(self, channel=None):
        """ Return the current state of the associated soft axis.
        """

        # Standard axis states:
        # MOVING : 'Axis is moving'
        # READY  : 'Axis is ready to be moved (not moving ?)'
        # FAULT  : 'Error from controller'
        # LIMPOS : 'Hardware high limit active'
        # LIMNEG : 'Hardware low limit active'
        # HOME   : 'Home signal active'
        # OFF    : 'Axis is disabled (must be enabled to move (not ready ?))'
        if channel.lower() != 'tsp':
            return AxisState("READY")
        else:
            if not self._moving or self._axes_tolerance[channel] == 0:
                return AxisState('READY')
            self._moving = self._eurosp_ismoving(channel)
            if self._moving:
                return AxisState('MOVING')
            else:
                return AxisState('READY')
            
    def _eurosp_ismoving(self, channel):
        wsp = self.read('WSP')/100
        pv = self.read('PV')/100
            
        band = abs(pv - self._sp)
        if band > self._axes_tolerance[channel]:
            self._inside = False
            return True
        else:
            if self._inside:
                dif = time.time() - self._inside
                if dif <= 0:
                    self._inside = time.time()
                    return True
                if dif >= self._time:
                    return False
            else:
                self._inside = time.time()
                return True
            
    def _create_sampling_counters(self, config, export_to_session=True):
        """
        """
        cnts_conf = config.get("counters")
        if cnts_conf is None:
            self._cc = None
            self._calc_counter = None
            return

        # create the sampling counters
        self._cc = PressureCellCounterController(self.name, self)

        for conf in cnts_conf:
            name = conf["counter_name"].strip()
            chan = conf["channel"].strip().upper()
            mode = conf.get("mode", "SINGLE")
            factor = conf.get("factor", 100)
            unit = conf.get("unit")
            cnt = self._cc.create_counter(SamplingCounter, name, mode=mode, unit=unit)
            cnt.channel = chan
            cnt.factor = factor

            if export_to_session:
                if current_session is not None:
                    if (
                        name in current_session.config.names_list
                        or name in current_session.env_dict.keys()
                    ):
                        raise ValueError(
                            f"Cannot export object to session with the name '{name}', name is already taken! "
                        )

                    current_session.env_dict[name] = cnt



class PressureCellCounterController(SamplingCounterController):
    def __init__(self, name, prescell):
        super().__init__(name)
        self.prescell = prescell

    def read_all(self, *counters):
        values = []
        for cnt in counters:
            values.append(self.prescell.read(cnt.channel)/cnt.factor)
        return values

    def read(self, counter):
        return self.prescell.read(counter.channel)/counter.factor
