import numpy as np

from bliss.controllers.counter import CalcCounterController


class MedianCalcCounterController(CalcCounterController):
    def calc_function(self, input_dict):
        values = [v for v in input_dict.values()]
        values = [v for v in values if v < 1e9]
        med = np.median(np.array(values))
        return {"roi_median": med}
