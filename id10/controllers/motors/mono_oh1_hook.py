from bliss.config.settings import HashSetting
from bliss.common.hook import MotionHook
# Si111 d-spacing
ds_111 = 3.1356

# Si311 d-spacing
ds_311 = 1.6375


class ChangeDspaceHook(MotionHook):
    """
     With the help of ccmy motor position, the right
     dspace is set. 
     When ccmy > 0, then Si311 crystal is in the beam
     else                Si111 crystal is in the beam
    """
    def __init__(self, name, config):
        super().__init__()
        self._energy_mot = None
        #print("In the constructor of ChangeDspaceHook")
        self._settings = HashSetting("%s" % name)
        self._settings["delta311"] = self._settings.get("delta311", config.get("delta311"))


    def set_energy_motor(self, axis):
        self._energy_mot = axis
        self._mono_mot = self._energy_mot.controller._tagged['monoang'][0]
        self._apply_delta311()


    def _apply_delta311(self, delta311=None):
        dspace = self._energy_mot.settings.get('dspace')
        if dspace == ds_311:
            try:
                delta311 = float(delta311 or self._settings.get("delta311"))
            except TypeError:
                raise RuntimeError("Delta311 not configured")

            self._mono_mot.no_offset = False
            self._mono_mot.position = self._mono_mot.dial + delta311
            self._settings["delta311"] = delta311
        else:
            self._mono_mot.no_offset = True
            self._mono_mot.position = self._mono_mot.dial



    def _get_delta311(self):
        dspace = self._energy_mot.settings.get('dspace')
        if dspace == ds_311:
            delta311 = self._settings["delta311"]
        else:
            print("CC mono1 is in Si111 position --> delta311 = 0")
            delta311 = 0.0
        return(delta311)


    def _get_dspace(self):
        dspace = self._energy_mot.settings.get('dspace')
        return(dspace)


    def pre_move(self, motion_list):
        if self._energy_mot is None:
            raise ValueError("energy motor not set")
        motion = motion_list[0]
        axis = motion.axis
        print("pre_move",axis.name,motion.target_pos)
        if motion.target_pos > 0:
            self._energy_mot.settings.set('dspace',ds_311)
            self._mono_mot.no_offset = False
            try:
                self._mono_mot.position = self._mono_mot.dial + self._settings["delta311"]
            except KeyError:
                raise RuntimeError("Delta311 not configured")
        else:
            self._energy_mot.settings.set('dspace',ds_111)
            self._mono_mot.no_offset = True
            self._mono_mot.position = self._mono_mot.dial
