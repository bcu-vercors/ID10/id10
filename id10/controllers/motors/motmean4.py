"""
ID10 pseudo motor which position is the arithmetic sum 
of the positions of 4 real motors

Configuration parameters:

* mot1: alias for 1st real axis
* mot2: alias for 2nd real axis
* mot3: alias for 3rd real axis
* mot4: alias for 4th real axis

* motmean: alias for calculated axis

Example of configuration file (motmean.yml) from ID10:
------------------------------------------------------

# Here is the config for arithmetic mean motor:
#
controller:
- class: motmean4
  package: id10.controllers.motors.motmean4
  axes:
  - name: $jur
    tags: real mot1
  - name: $jul
    tags: real mot2
  - name: $jdr
    tags: real mot3
  - name: $jdl
    tags: real mot4
  - name: tab4z
    tags: motmean4

"""

from bliss.controllers.motor import CalcController
import math


class motmean4(CalcController):
    def __init__(self, *args, **kwargs):
        CalcController.__init__(self, *args, **kwargs)

    def calc_from_real(self, positions_dict):
        mot1 = positions_dict["mot1"]
        mot2 = positions_dict["mot2"]
        mot3 = positions_dict["mot3"]
        mot4 = positions_dict["mot4"]    

        return {
            "motmean4": (mot1 + mot2 + mot3 + mot4)/4.
        }

    def calc_to_real(self, positions_dict):
        motmean = positions_dict["motmean4"]
       
        m1 = self._tagged["mot1"][0].position
        m2 = self._tagged["mot2"][0].position
        m3 = self._tagged["mot3"][0].position
        m4 = self._tagged["mot4"][0].position

        deltamean = motmean - (m1 + m2 + m3 + m4) / 4. 
        mot1 = m1 + deltamean
        mot2 = m2 + deltamean
        mot3 = m3 + deltamean
        mot4 = m4 + deltamean
        
        return {
            "mot1": mot1,
            "mot2": mot2,
            "mot3": mot3,
            "mot4": mot4
        }
