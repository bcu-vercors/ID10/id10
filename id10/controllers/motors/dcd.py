# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) 2016 Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

"""
Double Crystal Deflector support

(for details see the drawing on:
 wikiserv.esrf.fr/id10/images/b/b5/Diffractometer_Deflector_general_drawing_2016_04_22_names.pdf)

There are the following physical/real motors:

th1d  ... rotation of the 1st crystal (Ge111) which corresponds to the 
          Bragg angle at given X-ray energy
th2d  ... rotation of the 2nd crystal (Ge220) which corresponds to the 
          Bragg angle at given X-ray energy
rhod  ... rotation angle around X-axis of the assembly of 2 crystals
delta ... rotation angle around Z-axis of the sample support 
          (used OPTIONALLY)
phid  ... rotation angle around Z-axis of the assembly rhod 'ring' + 
          the 2 crystals (G111 = Ge220) support. (used OPTIONALLY when
          want to use correction file)

and calculated motor:

mu    ... angle between X-ray and the surface of the liquid sample
          and it can vary between 0 and th30 depending on the rhod angle.
          th30 is deflection angle with respect to the initial 
          beam direction achieved by both crystals th30 = 2(th2d-th1d)

Configuration parameters:

* dcd_l: distance (along X-axis) between the center of the 1st crystal
         and the sample [mm]
* dcd_th30: maximum deflection angle in degrees at given cc mono energy [degrees]
* dcd_delflag: flag which tells if motor delta (eh1_delta) is to be used
               or not; default = False. It is automatically set to True when
               coupling between mu and rhod is established (is set ON).
* dcd_corr_path: path under ~blissadm/local/beamline_configuration
                        where the correction file is
* dcd_corr_file: name of the file containing correction table 
* dcd_corr: flag which tells if correction table (involving phid motor)
            is on or off; default = False


Example of configuration file (dcd.yml) from ID10:
---------------------------------------------------

controller:
- class: dcd
  package: id10.controllers.motors.dcd
  dcd_l: 652.     
  dcd_th30: 12.0    
  dcd_delflag: False
  dcd_corr: False 
  axes:
  - name: $eccmono
    tags: real energy_ccmono
  - name: $th1d
    tags: real th1d
  - name: $th2d
    tags: real th2d
  - name: $rhod
    tags: real rhod 
  - name: $delta_eh1
    tags: real delta
  - name: $phid
    tags: real phid 
  #
  - name: mu
    tags: mu 

"""

import time
from numpy import *

from bliss.config import settings
from bliss.common.utils import object_method
from bliss.common.logtools import log_debug, log_info, log_warning
from bliss.controllers.motor import CalcController
from bliss.common.utils import RED

class dcd(CalcController):
    def __init__(self, *args, **kwargs):
        #CalcController.__init__(self, *args, **kwargs)
        super().__init__(*args, **kwargs)

        log_info(self, "In dcd constructor")

        ctrl_name = self.config.get('name', default=None)
        log_info(self, "__init__: Controller name = %s" % ctrl_name)

        ##### --- DCD_L ---
        # SimpleSetting object
        self._dcd_l = settings.SimpleSetting('dcd_l', default_value=652.0)
        # need persistence for mu_to_set when dcd is off
        self._mu_to_set = settings.SimpleSetting('mu_to_set', default_value=0)
        
        # get value from simple settings object
        _dcd_l = self._dcd_l.get()    # this one is of type float
        if _dcd_l == None:
            log_debug(self, "__init__: dcd_l does not exist in settings")
            log_debug(self, "__init__: try to get it from config")
            _dcd_l = self.config.get('dcd_l', float)
            if _dcd_l == None:
                log_debug(self, "__init__: dcd_l does not exist in config either")
                return
            else:
                # Put value found in config to settings
                log_debug(self, "__init__: dcd_l from config = %f" % _dcd_l)
                log_debug(self, "__init__: --> will put it in settings")
                self._dcd_l.set(_dcd_l)
        else:
            log_debug(self, "__init__: dcd_l from settings = %f" % _dcd_l)
            # get it also from config to compare (it could have been 
            # changed in config).
            _dcd_l_conf = self.config.get('dcd_l', float)
            if _dcd_l_conf == None:
                log_debug(self, "__init__: dcd_l does not exist in config --> keep the value from settings")
            else:
                if _dcd_l_conf != _dcd_l:
                    print("dcd_l from config %f is different from the one in settings %f" % (_dcd_l_conf, _dcd_l))
                    ans = input("Want to apply the one from config?").lower()
                    if 'y' in ans:
                       _dcd_l = _dcd_l_conf
                       self._dcd_l.set(_dcd_l)
        self.dcd_l = _dcd_l
        if (self.dcd_l <= 600) or (self.dcd_l >= 700):
            raise ValueError("Bad value %f of 1st_crystal-sample distance; should be in [600,700]" % self.dcd_l) 

        ##### --- DCD_TH30 ---
        # SimpleSetting object
        self._dcd_th30 = settings.SimpleSetting('dcd_th30', default_value=12.0)
        # get value from simple settings object
        _dcd_th30 = self._dcd_th30.get()
        if _dcd_th30 == None:
            log_debug(self, "__init__: dcd_th30 does not exist in settings")
            log_debug(self, "__init__: try to get it from config")
            _dcd_th30 = self.config.get('dcd_th30', float)
            if _dcd_th30 == None:
                log_debug(self, "__init__: dcd_th30 does not exist in config either")
                return
            else:
                # Put value found in config to settings
                log_debug(self, "__init__: dcd_th30 from config = %f" % _dcd_th30)
                log_debug(self, "__init__: --> will put it in settings")
                self._dcd_th30.set(_dcd_th30)
        else:
            log_debug(self, "__init__: dcd_th30 from settings = %f" % _dcd_th30)
            # get it also from config to compare (it could have been 
            # changed in config).
            _dcd_th30_conf = self.config.get('dcd_th30', float)
            if _dcd_th30_conf == None:
                log_debug(self, "__init__: dcd_th30 does not exist in config --> keep the value from settings")
            else:
                if _dcd_th30_conf != _dcd_th30:
                    print("dcd_th30 from config %f is different from the one in settings %f" % (_dcd_th30_conf, _dcd_th30))
                    ans = input("Want to apply the one from config?").lower()
                    if 'y' in ans:
                       _dcd_th30 = _dcd_th30_conf
                       self._dcd_th30.set(_dcd_th30)
        self.dcd_th30 = _dcd_th30
 
        ##### --- DCD_DELFLAG ---
        # SimpleSetting object
        self._dcd_delflag = settings.SimpleSetting('dcd_delflag', default_value=False)
        # get value from simple settings object
        _dcd_delflag = self._dcd_delflag.get()
        if _dcd_delflag == None:
            log_debug(self, "__init__: dcd_delflag does not exist in settings")
            log_debug(self, "__init__: try to get it from config")
            _dcd_delflag = self.config.get('dcd_delflag', bool)
            if _dcd_delflag == None:
                log_debug(self, "__init__: dcd_delflag does not exist in config either")
                return
            else:
                # Put value found in config to settings
                log_debug(self, "__init__: dcd_delflag from config = %r" % _dcd_delflag)
                log_debug(self, "__init__: --> will put it in settings")
                self._dcd_delflag.set(_dcd_delflag)
        else:
            log_debug(self, "__init__: dcd_delflag from settings = %r" % _dcd_delflag)
            # get it also from config to compare (it could have been 
            # changed in config).
            _dcd_delflag_conf = self.config.get('dcd_delflag', bool)
            if _dcd_delflag_conf == None:
                log_debug(self, "__init__: dcd_delflag does not exist in config --> keep the value from settings")
            else:
                if _dcd_delflag_conf != _dcd_delflag:
                    print("dcd_delflag from config %r is different from the one in settings %r" % (_dcd_delflag_conf, _dcd_delflag))
                    ans = input("Want to apply the one from config?").lower()
                    if 'y' in ans:
                       _dcd_delflag = _dcd_delflag_conf
                       self._dcd_delflag.set(_dcd_delflag)
        self.dcd_delflag = _dcd_delflag

        ##### --- Correction table file path (= part of the path
        #####     under ~blissadm/local/beamline_configuation)
        # SimpleSetting object
        self._dcd_corr_path = settings.SimpleSetting('dcd_corr_path', default_value="")
        # get value from simple settings object
        _dcd_corr_path = self._dcd_corr_path.get()
        if _dcd_corr_path == "":
            log_debug(self, "__init__: dcd_corr_path does not exist in settings")
            log_debug(self, "__init__: try to get it from config")
            _dcd_corr_path = self.config.get('dcd_corr_path', str)
            if _dcd_corr_path == "":
                log_debug(self, "__init__: dcd_corr_path does not exist in config either")
                return
            else:
                # Put value found in config to settings
                log_debug(self, "__init__: dcd_corr_path from config = %s" % _dcd_corr_path)
                log_debug(self, "__init__: --> will put it in settings")
                self._dcd_corr_path.set(_dcd_corr_path)
        else:
            log_debug(self, "__init__: dcd_corr_path from settings = %s" % _dcd_corr_path)
            # get it also from config to compare (it could have been 
            # changed in config).
            _dcd_corr_path_conf = self.config.get('dcd_corr_path', str)
            if _dcd_corr_path == "":
                log_debug(self, "__init__: dcd_corr_path does not exist in config --> keep the value from settings")
            else:
                if _dcd_corr_path_conf != _dcd_corr_path:
                    print("dcd_corr_path from config %r is different from the one in settings %r" % (_dcd_corr_path_conf, _dcd_corr_path))
                    ans = input("Want to apply the one from config?").lower()
                    if 'y' in ans:
                       _dcd_corr_path = _dcd_corr_path_conf
                       self._dcd_corr_path.set(_dcd_corr_path)
        self.dcd_corr_path = _dcd_corr_path

        ##### --- Correction table file name
        # SimpleSetting object
        self._dcd_corr_file = settings.SimpleSetting('dcd_corr_file', default_value="dcdcor.conf")
        # get value from simple settings object
        _dcd_corr_file = self._dcd_corr_file.get()
        if _dcd_corr_file == "":
            log_debug(self, "__init__: dcd_corr_file does not exist in settings")
            log_debug(self, "__init__: try to get it from config")
            _dcd_corr_file = self.config.get('dcd_corr_file', str)
            if _dcd_corr_file == None:
                log_debug(self, "__init__: dcd_corr_file does not exist in config either")
                return
            else:
                # Put value found in config to settings
                log_debug(self, "__init__: dcd_corr_file from config = %s" % _dcd_corr_file)
                log_debug(self, "__init__: --> will put it in settings")
                self._dcd_corr_file.set(_dcd_corr_file)
        else:
            log_debug(self, "__init__: dcd_corr_file from settings = %s" % _dcd_corr_file)
            # get it also from config to compare (it could have been 
            # changed in config).
            _dcd_corr_file_conf = self.config.get('dcd_corr_file', str)
            if _dcd_corr_file_conf == "":
                log_debug(self, "__init__: dcd_corr_file does not exist in config --> keep the value from settings")
            else:
                if _dcd_corr_file_conf != _dcd_corr_file:
                    print("dcd_corr_file from config %r is different from the one in settings %r" % (_dcd_corr_file_conf, _dcd_corr_file))
                    ans = input("Want to apply the one from config?").lower()
                    if 'y' in ans:
                       _dcd_corr_file = _dcd_corr_file_conf
                       self._dcd_corr_file.set(_dcd_corr_file)
        self.dcd_corr_file = _dcd_corr_file

        ##### --- DCDCOR_ON ---
        # SimpleSetting object
        self._dcd_corr = settings.SimpleSetting('dcd_corr', default_value=False)
        # get value from simple settings object
        _dcd_corr = self._dcd_corr.get()
        if _dcd_corr == None:
            log_debug(self, "__init__: dcd_corr does not exist in settings")
            log_debug(self, "__init__: try to get it from config")
            _dcd_corr = self.config.get('dcd_corr', bool)
            if _dcd_corr == None:
                log_debug(self, "__init__: dcd_corr does not exist in config either")
                return
            else:
                # Put value found in config to settings
                log_debug(self, "__init__: dcd_corr from config = %r" % _dcd_corr)
                log_debug(self, "__init__: --> will put it in settings")
                self._dcd_corr.set(_dcd_corr)
        else:
            log_debug(self, "__init__: dcd_corr from settings = %r" % _dcd_corr)
            # get it also from config to compare (it could have been 
            # changed in config).
            _dcd_corr_conf = self.config.get('dcd_corr', bool)
            if _dcd_corr_conf == None:
                log_debug(self, "__init__: dcd_corr does not exist in config --> keep the value from settings")
            else:
                if _dcd_corr_conf != _dcd_corr:
                    print("dcd_corr from config %r is different from the one in settings %r" % (_dcd_corr_conf, _dcd_corr))
                    ans = input("Want to apply the one from config?").lower()
                    if 'y' in ans:
                       _dcd_corr = _dcd_corr_conf
                       self._dcd_corr.set(_dcd_corr)
        self.dcd_corr = _dcd_corr

        # By default coupling between real motors and calculated motor
        # is OFF. Can set it ON with dcd_on function.
        self._dcd_on = False
        log_debug(self, "__init__: dcd_on = %r" % self._dcd_on)

        # Set both offsets to 0. Will be calculated in the
        # function dcd_calc_offsets() invoked in dcd_on().
        self.rhod_offset = 0
        self.delta_offset = 0
        self.theta_offset = 0

        self.dcd_thetaflag = False
        
    # ---------- initialize() -------------
    def initialize(self):
        super().initialize()
        log_info(self, "In initialize")
        #print("\nIn initialize")
        #print("=============")

        # Get references to motor objects
        self.energy_ccmono = self._tagged['energy_ccmono'][0]
        self.th1d = self._tagged['th1d'][0]
        self.th2d = self._tagged['th2d'][0]
        self.rhod = self._tagged['rhod'][0]
        self.delta = self._tagged['delta'][0]
        self.theta = self._tagged['theta'][0]
        self.phid = self._tagged['phid'][0]
        self.mu = self._tagged['mu'][0]
        
        # Log the initial real motor positions
        # Remark: cant get here position of calculcated (= mu)
	#         motor, since not yet initialized
        log_debug(self, "initialize: Initial energy = %f" % self.energy_ccmono.position)
        log_debug(self, "initialize: Initial th1d position = %f" % self.th1d.position)
        log_debug(self, "initialize: Initial th2d position = %f" % self.th2d.position)
        log_debug(self, "initialize: Initial rhod position = %f" % self.rhod.position)
        log_debug(self, "initialize: Initial delta position = %f" % self.delta.position)
        log_debug(self, "initialize: Initial phid position = %f" % self.phid.position)

        # Remark: We defer the offset calculation to the function
        #         dcd_on(), since here we do not have yet valid mu
        #         position. 

        log_debug(self, "initialize: dcd_corr flag = %r" % self.dcd_corr)
        if self.dcd_corr == True:
            # Load correction table values if demanded
            self._dcd_corr_load()
            # Sort the lines in ascending order of the 1st field (= mu start pos.)
            nblines = self._dcd_corr_sort()

        if True: #(self.dcd_th30 <= 4) or (self.dcd_th30 >= 22):
            # Calculate th30 value ONLY IF the value from config or/and 
            # settings IS OUT OF LIMITS.
            #log_warning(self, "__init__: dcd_th30 %f out of range [4,22]" % self.dcd_th30)
            #log_warning(self, "__init__: Will calculate it")
            # Calculate maximum deflection angle
            self.dcd_calc_th30()


    # ---------- initialize_axis() -------------
    def initialize_axis(self, axis):
        log_info(self, "In initialize_axis")
        CalcController.initialize_axis(self, axis)


 
    # ---------- dcd_calc_th30() -------------
    def dcd_calc_th30(self):
        """
        Calculate maximum deflection angle
        Called from initialize() function of dcd controller
        """

        log_info(self, "In dcd_calc_th30")

        self.energy = self.energy_ccmono.position
        p1 = (12.3985/self.energy)*sqrt(3)/(2*5.65735)
        p2 = (12.3985/self.energy)*sqrt(8)/(2*5.65735)

        # Bragg angle in degrees of the 1st crystal = Ge(111)
        _th1d = rad2deg(arcsin(p1))
        # Bragg angle in degrees of the 2nd crystal = Ge(220)
        _th2d = rad2deg(arcsin(p2))

        # Maximum deflection angle in degrees
        self.dcd_th30 = 2*(_th2d - _th1d)

        log_debug(self, "dcd_calc_th30: 1st crystal Bragg angle = %f degrees" % _th1d)
        log_debug(self, "dcd_calc_th30: 2nd crystal Bragg angle = %f degrees" % _th2d)
        log_debug(self, "dcd_calc_th30: Maximum deflection angle = %f degrees" % self.dcd_th30)



    # ---------- dcd_calc_offsets() -------------
    def dcd_calc_offsets(self):
        """
         Calculate offset of rhod and optionally of delta 
        """

        log_info(self, "In dcd_calc_offsets")
        #print("\nIn dcd_calc_offsets")
        #print("===================")
    
        log_debug(self, "dcd_calc_offsets:  rhod position = %f" % self.rhod.position) 
        log_debug(self, "dcd_calc_offsets: delta position = %f" % self.delta.position) 
        log_debug(self, "dcd_calc_offsets:    mu position = %f" % self.mu.position) 
        log_debug(self, "dcd_calc_offsets: max th30 angle = %f" % self.dcd_th30)
   
        if self._dcd_on == True: 
            self.rhod_offset = self.rhod.position + rad2deg( arcsin (sin(deg2rad(self.mu_to_set))/sin(deg2rad(self.dcd_th30)) ) )
            log_debug(self, "rhod offset = %f" % self.rhod_offset)
            log_debug(self, "dcd_calc_offsets: rhod offset = %f" % self.rhod_offset) 

            if self.dcd_delflag == True:
                self.delta_offset = self.delta.position - rad2deg( arctan (tan(deg2rad(self.dcd_th30)) * cos(deg2rad(self.rhod.position)) ) )
                log_debug(self, "delta offset = %f" % self.delta_offset)
                log_debug(self, "dcd_calc_offsets: delta offset = %f" % self.delta_offset)
                if self.dcd_thetaflag:
                    self.theta_offset = self.theta.position - rad2deg( arctan (tan(deg2rad(self.dcd_th30)) * cos(deg2rad(self.rhod.position)) ) )
                    



    # ---------- dcd_ca() -------------
    def dcd_ca(self, mupos, th3opt=None):
        """
         Auxiliary function which does some useful calculations
        """

        log_info(self, "In dcd_ca: mupos = %f" % mupos)
        if th3opt != None:
            log_info(self, "In dcd_ca: th3opt = %f" % th3opt)

        # Calculate offsets here to not depend on the order
        # of calls (i.e. we do not need to have called dcd_on
        # before). 
        _orig_dcdon = self._dcd_on
        if self._dcd_on == False:
            # set it temporarily to True to calculate also
            # delta offset needed below
            self._dcd_on = True
            log_debug(self, "dcd_ca: set dcd_on to True")

        # Calculate offsets here to not depend on the order
        # of calls (i.e. we do not need to have called dcd_on
        # before). And even if it was called before we do not
        # have delta_offset in case dcd_delflag is False, but we
        # we need it in the estimations/calculations below.
        _orig_delflag = self.dcd_delflag
        if self.dcd_delflag == False:
            # set it temporarily to True to calculate also
            # delta offset needed below
            self.dcd_delflag = True
            log_debug(self, "dcd_ca: set dcd_delflag to True")

        # inside calc_offsets() we use dcd_on dcd_delflag flags
        #self.dcd_calc_offsets()

        self.energy = self.energy_ccmono.position

        p1 = (12.3985/self.energy)*sqrt(3)/(2*5.65735)
        p2 = (12.3985/self.energy)*sqrt(8)/(2*5.65735)

        # Bragg angle in degrees of the 1st crystal = Ge(111)
        _th1d = rad2deg(arcsin(p1))
        # Bragg angle in degrees of the 2nd crystal = Ge(220)
        _th2d = rad2deg(arcsin(p2))

        # Maximum deflection angle in degrees
        #_th30 = 2*(_th2d - _th1d)


        # Distance along X-axis between centers of 2 crystals
        Xshift = self.dcd_l * tan(deg2rad(self.dcd_th30)) / ( tan(deg2rad(2*_th1d)) + tan(deg2rad(self.dcd_th30)) )

        # Distance along Y-axis between centers of 2 crystals
        Yshift = Xshift * tan(deg2rad(2*_th1d))

        # Rotation angle around X-axis of double crystal assembly in degrees
        _rhod = self.rhod_offset - rad2deg( arcsin( sin(deg2rad(mupos))/sin(deg2rad(self.dcd_th30)) ) )

        # Reflected beam angular position in degrees
        # = rotation angle around Z-axis of the sample support
        _delta = self.delta_offset + rad2deg( arctan( tan(deg2rad(self.dcd_th30)) * cos(deg2rad(_rhod-self.rhod_offset)) ) )


        # added by OK, Qz calculation
        _Qz = 4.0*pi*sin(deg2rad(mupos))/(12.3985/self.energy)


        # added by OK, Theoretical, without offset on motors: Rotation angle around X-axis of double crystal assembly in degrees
        _rhod_0offset = - rad2deg( arcsin( sin(deg2rad(mupos))/sin(deg2rad(self.dcd_th30)) ) )

        # added by OK, Theoretical, without offset on motors: Reflected beam angular position in degrees
        # = rotation angle around Z-axis of the sample support
        _delta_0offset = + rad2deg( arctan( tan(deg2rad(self.dcd_th30)) * cos(deg2rad(_rhod_0offset)) ) )
	
	

        if th3opt != None:
            # Distance along X-axis between centers of 2 crystals
            Xshiftopt = self.dcd_l * tan(deg2rad(th3opt)) / ( tan(deg2rad(2*_th1d)) + tan(deg2rad(th3opt)) )

            # Distance along Y-axis between centers of 2 crystals
            Yshiftopt = Xshiftopt * tan(deg2rad(2*_th1d))

            # Rotation angle around X-axis of double crystal 
            # assembly in degrees
            _rhodopt = self.rhod_offset - rad2deg( arcsin( sin(deg2rad(mupos))/sin(deg2rad(th3opt)) ) )

            # Reflected beam angular position in degrees
            # = rotation angle around Z-axis of the sample support
            _deltaopt = self.delta_offset + rad2deg( arctan( tan(deg2rad(th3opt)) * cos(deg2rad(_rhodopt)) ) )

        print("\ndcd_ca: MU value = %8.4f\t\t Qz = %8.4f" % (mupos,_Qz))
        print("dcd_ca: Energy = %8.4f\t\t th3 = %8.4f" % (self.energy,self.dcd_th30))
        print("dcd_ca: th1d = %8.4f\t\t th2d = %8.4f" % (_th1d, _th2d))
        print("dcd_ca: Xshift = %8.3f\t Yshift = %8.3f" % (Xshift, Yshift))
        print("dcd_ca: rhod = %f\t delta = %f" % (_rhod, _delta))
        print("dcd_ca: rhod0 = %f\t delta0 = %f" % (_rhod_0offset, _delta_0offset))
        if th3opt != None:
            print("\ndcd_ca: Input th3opt = %f" % th3opt)
            print("dcd_ca: Xshiftopt = %f\t\t Yshiftopt = %f" % (Xshiftopt, Yshiftopt))
            print("dcd_ca: rhodopt = %f\t\t deltaopt = %f" % (_rhodopt, _deltaopt))

        if _orig_delflag == False:
            self.dcd_delflag = False
            log_debug(self, "dcd_ca: set dcd_delflag back to False")

        if _orig_dcdon == False:
            self._dcd_on = False
            log_debug(self, "dcd_ca: set dcd_on back to False")

    

    # ---------- rhodtomu() -------------
    def rhodtomu(self, rhodpos):
        log_info(self, "In rhodtomu")
        log_debug(self, f"rhodtomu: rhod position = {rhodpos}")
        log_debug(self, f"rhodtomu: rhod offset   = {self.rhod_offset}")
        log_debug(self, f"rhodtomu: th30          = {self.dcd_th30}")
        mu_pos = rad2deg (arcsin (sin(deg2rad(self.dcd_th30)) * sin(deg2rad(self.rhod_offset - rhodpos)) ) )
        log_debug(self, f"rhodtomu: mu position = {mu_pos}")
        return(mu_pos)



    # ---------- mutorhod() -------------
    def mutorhod(self, mupos):
        log_info(self, "In mutorhod")
        log_debug(self, f"mutorhod: mu position = {mupos}")
        log_debug(self, f"mutorhod: rhod offset   = {self.rhod_offset}")
        log_debug(self, f"mutorhod: th30        = {self.dcd_th30}")
        rhod_pos = self.rhod_offset - rad2deg( arcsin( sin(deg2rad(mupos)) / sin(deg2rad(self.dcd_th30)) ) )
        log_debug(self, f"mutorhod: rhod position = {rhod_pos}")
        return(rhod_pos)



    # ---------- rhodtodelta() -------------
    def rhodtodelta(self, rhodpos):
        log_info(self, "In rhodtodelta")
        log_debug(self, f"rhodtodelta: rhod position = {rhodpos}")
        log_debug(self, f"rhodtodelta: delta offset  = {self.delta_offset}")
        log_debug(self, f"rhodtodelta: th30          = {self.dcd_th30}")
        delta_pos= self.delta_offset + rad2deg (arctan (tan(deg2rad(self.dcd_th30)) * cos(deg2rad(rhodpos-self.rhod_offset)) ) ) 
        log_debug(self, f"rhodtodelta: delta position = {delta_pos}")
        return(delta_pos)

    # ---------- rhodtotheta() -------------
    def rhodtotheta(self, rhodpos):
        log_info(self, "In rhodtotheta")
        log_debug(self, f"rhodtotheta: rhod position = {rhodpos}")
        log_debug(self, f"rhodtotheta: theta offset  = {self.theta_offset}")
        log_debug(self, f"rhodtotheta: th30          = {self.dcd_th30}")
        theta_pos= self.theta_offset + rad2deg (arctan (tan(deg2rad(self.dcd_th30)) * cos(deg2rad(rhodpos-self.rhod_offset)) ) ) 
        log_debug(self, f"rhodtotheta: theta position = {theta_pos}")
        return(theta_pos)



    # ---------- deltatorhod() -------------
    def deltatorhod(self, deltapos):
        log_info(self, "In deltatorhod")
        log_debug(self, f"deltatorhod: delta position = {deltapos}")
        log_debug(self, f"deltatorhod: delta offset   = {self.delta_offset}")
        log_debug(self, f"deltatorhod: th30           = {self.dcd_th30}")
        # Very important!!!!
        # In next formula must put negative sign in front,
        # otherwise the sign of the calculated rhod position is wrong!!!
        rhod_pos = self.rhod_offset -rad2deg (arccos (tan(deg2rad(deltapos - self.delta_offset)) / tan(deg2rad(self.dcd_th30)) ) ) 
        log_debug(self, f"deltatorhod: rhod position = {rhod_pos}")
        return(rhod_pos)




    # ---------- calc_from_real() -------------
    def calc_from_real(self, positions_dict):

        log_info(self, "calc_from_real()")

        #print(positions_dict)
        #print("calc_from_real: mu position (as read) = %f" % self.mu.position)
        log_debug(self, "calc_from_real: mu position (as read) = %f" % self.mu.position)

        # Remark:
	# In SPEC macro set id10dcd.mac the position of mu
        # is never calculated from real motors !!
        # But the fact that this controller inherits from 
        # CalcController the relation between real and 
        # and calculated motors is always bidirectional.
        if self._dcd_on == True:
            log_debug(self, "calc_from_real(): Coupling between real motors (rhod (+ delta)) and mu is ON")
            if self.dcd_delflag == True:
                delta_pos = positions_dict["delta"]
                log_debug(self, "calc_from_real: delta position (from position dict.) = %f" % delta_pos)
                log_debug(self, "calc_from_real: rhod position (from position dict.) = %f" % positions_dict["rhod"])
                #rhod_pos = self.deltatorhod(delta_pos)
                rhod_pos = positions_dict["rhod"]
                log_debug(self, "calc_from_real: rhod position = %f (calc. from delta pos.)" % rhod_pos)
                # Very important!!! 
                # Had to add next 2 lines as well as the line that copies wanted destination 
                # mu position to the variable self.mu_to_set in calc_to_real().
                # If next 2 lines are missing, then when want to move mu to a negative value, 
                # in the end get the +ve one!!!
                # (this comes from the fact that deltatorhod always gives the same 
                #  sign of rhod position!!)
                if self.mu_to_set < 0:
                    rhod_pos = -rhod_pos
            else:
                rhod_pos = positions_dict["rhod"]
                log_debug(self, f"calc_from_real: rhod position (from position dict.) {rhod_pos}")
            mu_pos = self.rhodtomu(rhod_pos)
            log_debug(self, f"calc_from_real: mu position (calc. from rhod pos.) = {mu_pos}")
        else:
            log_debug(self, "calc_from_real(): Coupling between real motors (rhod (+ delta)) and mu is OFF")
            # take current mu position
            mu_pos = self.mu_to_set
            log_debug(self, f"mu position (as read) = {mu_pos}")

        return {"mu": mu_pos}



    # ---------- calc_to_real() -------------
    def calc_to_real(self, positions_dict):

        log_info(self, "calc_to_real()")

        #print(positions_dict)

        mu_pos = positions_dict["mu"]

        # Just read the current positions
        if not isinstance(mu_pos, ndarray):
            self.mu_to_set = mu_pos
        else:
            self.mu_to_set = mu_pos[-1]

        log_debug(self, f"calc_to_real: mu position (from position dict.) = {mu_pos}")

        # Read current delta position to have it in the end in the case
        # dcd_on = True and dcd_delflag = False
        delta_pos = self.delta.position
        theta_pos = self.theta.position
        
        if self._dcd_on == True:
            log_debug(self, "calc_to_real(): Coupling between real motors (rhod (+ delta)) and mu is ON")
            rhod_pos = self.mutorhod(mu_pos)
            log_debug(self, f"calc_to_real: rhod position (calc. from mu pos.) = {rhod_pos}")
            if self.dcd_delflag == True:
                delta_pos = self.rhodtodelta(rhod_pos)
                log_debug(self, f"calc_to_real: delta position (from rhod pos.) = {delta_pos}")
                if self.dcd_thetaflag == True:
                    theta_pos = self.rhodtotheta(rhod_pos)
                    log_debug(self, f"calc_to_real: theta position (from rhod pos.) = {theta_pos}")
                    
        else:
            # Check if user asked for a scan with DCD OFF
            if isinstance(mu_pos, ndarray):
                raise ValueError(RED("DCD is OFF, you cannot run a scan on mu motor"))
            
            log_debug(self, "calc_to_real(): Coupling between real motors (rhod (+ delta)) and mu is OFF")
            
            rhod_pos = self.rhod.position
            #delta_pos = self.delta.position
            log_debug(self, f"calc_to_real: rhod position (as read) = {rhod_pos}")
            if self.dcd_delflag == True:
                log_debug(self, f"calc_to_real: delta position (as read) = {delta_pos}")

        if self.dcd_corr == True:
            retpos = self._dcd_corr_apply(mu_pos)
            log_debug(self, "calc_to_real: After applying corrections")
            log_debug(self, "calc_to_real: th1d_pos = %f" % retpos[0])
            log_debug(self, "calc_to_real: th2d_pos = %f" % retpos[1])
            log_debug(self, "calc_to_real: phid_pos = %f" % retpos[2])

            return {"rhod": rhod_pos, "delta": delta_pos, "th1d": retpos[0], "th2d": retpos[1], "phid": retpos[2], "theta": theta_pos}

        #self.rhod.position = rhod_pos
        #self.delta.position = delta_pos

        return {"rhod": rhod_pos, "delta": delta_pos, "theta": theta_pos}        


    # ---------- _dcd_corr_load() -------------
    def _dcd_corr_load(self):
        """
        Get correction table from file
        """
        log_info(self, "_dcd_corr_load()")
        log_debug(self, "_dcd_corr_load(): path to correction table file = %s" % self.dcd_corr_path)

        if self.dcd_corr_path.startswith("/"):
            uptofile = "/users/blissadm/local/beamline_configuration"+self.dcd_corr_path
        else:
            uptofile = "/users/blissadm/local/beamline_configuration/" + self.dcd_corr_path

        if self.dcd_corr_path.endswith("/"):
            corr_fullpath = uptofile+self.dcd_corr_file
        else:
            corr_fullpath = uptofile+"/"+self.dcd_corr_file

        log_debug(self, "_dcd_corr_load(): Full correction table file path = %s" % corr_fullpath)
        self.corr_fullpath = corr_fullpath

        try:
            fd = open(self.corr_fullpath, "r")
        except IOError as e:
            print("I/O Error({0}): {1}".format(e.errno,e.strerror))
            return

        self.corr_header = []
        self.corr_values_unsorted = []
        # full correction table file contents
        contents = fd.readlines()
        for line in contents:
            if line.startswith("#"):
                self.corr_header.append(line)
            else:
                self.corr_values_unsorted.append(line)
        fd.close()
        #print(self.corr_header)
        #print(self.corr_values_unsorted)
        # corr_header as well as corr_values_unsorted are
        # list of strings.


    # ---------- _dcd_corr_sort() -------------
    def _dcd_corr_sort(self):
        """
        Sort correction table data in ascending order of the 1st field
        """
        log_info(self, "_dcd_corr_sort()")
        nblines = len(self.corr_values_unsorted)

        # Create list of lists i.e. each line in unsorted values is
        # converted from simple string to the list of values.
        aaa = []
        for i in range(nblines):
            aaa.append([float(j) for j in self.corr_values_unsorted[i].split()])

        # Sort in ascending order of the 1st field
        aaa.sort(key=lambda aaa: aaa[0])
     
        # Convert 3rd, 5th and 7th field in integers since these are flags
        for i in range(nblines):
            aaa[i][3] = int(aaa[i][3])
            aaa[i][5] = int(aaa[i][5])
            aaa[i][7] = int(aaa[i][7])

        # Remove possible duplicates
        i = 0
        while i < (nblines - 1):
            if aaa[i][0] == aaa[i+1][0]:
                for ival in range(i+1, nblines):
                    aaa[ival-1] = aaa[ival]
                nblines = nblines - 1
            else:
                i = i + 1

        self.corr_values_sorted = aaa

        #print("Sorted correction values\n")
        #for i in range(nblines):
        #    print(self.corr_values_sorted[i])

        return (nblines)


    # ---------- _dcd_corr_apply() -------------
    def _dcd_corr_apply(self, mupos):
        """
        Apply correction table to th1d, th2d, phid
        """
        log_info(self, "_dcd_corr_apply()")
        log_debug(self, f"_dcd_corr_apply: input mu position = {mupos}")

        # Get current positions for these 3 motors:
        th1d_pos = self.th1d.position
        th2d_pos = self.th2d.position
        phid_pos = self.phid.position

        #print("_dcd_corr_apply: Original th1d position = %f" % th1d_pos)
        #print("_dcd_corr_apply: Original th2d position = %f" % th2d_pos)
        #print("_dcd_corr_apply: Original phid position = %f" % phid_pos)
        #print("_dcd_corr_apply: Sorted corr values")
        nblines = len(self.corr_values_sorted)

        #for i in range(nblines):
        #    print(self.corr_values_sorted[i])
        #print(self.corr_values_sorted)

        for i in range(nblines):
            if (mupos >= self.corr_values_sorted[i][0]) and (mupos < self.corr_values_sorted[i][1]):
                if self.corr_values_sorted[i][3] != -1:
                    th1d_pos = th1d_pos * self.corr_values_sorted[i][3] + self.corr_values_sorted[i][2]
                if self.corr_values_sorted[i][5] != -1:
                    th2d_pos = th2d_pos * self.corr_values_sorted[i][5] + self.corr_values_sorted[i][4]
                if self.corr_values_sorted[i][7] != -1:
                    print("phid flag is not -1")
                    phid_pos = phid_pos * self.corr_values_sorted[i][7] + self.corr_values_sorted[i][6]
                break
        #self.th1d.position = th1d_pos
        #self.th2d.position = th2d_pos
        #self.phid.position = phid_pos
        log_debug(self, f"_dcd_corr_apply: Corrected phid position = {phid_pos}")
        return(th1d_pos, th2d_pos, phid_pos)


    
    # Some object methods574,
    # ===================

    # ---------- dcd_on() -------------
    @object_method
    def dcd_on(self,_ = None):
        """
         Set mu to rhod coupling ON 
         AND (on request of Oleg) also
         set delta to rhod coupling ON
        """
   
        log_info(self, "In dcd_on")
        self._dcd_on = True
        self.dcd_delflag = True   # Oleg wants this one to be True here
        self.dcd_calc_offsets()


    # ---------- dcd_off() -------------
    @object_method
    def dcd_off(self,_=None):
        """
         Set mu to rhod coupling OFF
         and also delta to rhod couplinf OFF
	 since it makes no sense to keep delta coupling ON
         if mu is not coupled to rhod.
         Also set corrections OFF since they make no sense
         when mu to rhod are not coupled.
        """
   
        log_info(self, "In dcd_off")
        self._dcd_on = False 
        self.dcd_delflag = False 
        self.rhod_offset = 0
        self.delta_offset = 0
        if self.dcd_corr == True:
            self.dcd_corr = False

    def theta_on(self):
        self.dcd_thetaflag=True
        
    def theta_off(self): 
        self.dcd_thetaflag=False
    
    # ---------- is_dcd_on() -------------
    @object_method
    def is_dcd_on(self,_ = bool):
        """
         Tells if mu to rhod (and also delta to rhod) coupling is ON or OFF
        """
   
        log_info(self, "In is_dcd_on")
        return(self._dcd_on)



    # ---------- dcd_delta_on() -------------
    @object_method
    def dcd_delta_on(self,_ = None):
        """
         Set delta to rhod coupling ON
         With this command can 'override' the value
         of the flag dcd_delflag from config/settings
         or as was set with dcd_on()
        """
   
        log_info(self, "In dcd_delta_on")
        self.dcd_delflag = True
        self.dcd_calc_offsets()


    # ---------- dcd_delta_off() -------------
    @object_method
    def dcd_delta_off(self,_ = None):
        """
         Set delta to rhod coupling OFF
         With this command can 'override' the value
         of the flag dcd_delflag from config/settings
         or as was set with dcd_on()
        """
   
        log_info(self, "In dcd_delta_off")
        self.dcd_delflag = False 


    # ---------- is_delta_on() -------------
    @object_method
    def is_delta_on(self,_ = bool):
        """
         Tells if delta to rhod coupling is ON or OFF
        """
   
        log_info(self, "In is_delta_on")
        return(self.dcd_delflag)



    # ---------- dcd_corr_on() -------------
    @object_method
    def dcd_corr_on(self,_ = None):
        """
         Set th1d, th1d and phid correction ON
         With this command can 'override' the value
         of the flag dcd_corr from config
         Setting this correction consists of reading of correction file.
        """
   
        log_info(self, "In dcd_corr_on")
        self.dcd_corr = True
        # Load correction table values
        self._dcd_corr_load()
        # Sort the lines in ascending order of the 1st field (= mu start pos.)
        nblines = self._dcd_corr_sort()


    # ---------- dcd_corr_off() -------------
    @object_method
    def dcd_corr_off(self,_ = None):
        """
         Set th1d, th1d and phid correction OFF
         With this command can 'override' the value
         of the flag dcd_corr from config
        """
   
        log_info(self, "In dcd_corr_off")
        self.dcd_corr = False 


    # ---------- is_corr_on() -------------
    @object_method
    def is_corr_on(self,_ = bool):
        """
         Tells if correction th1d, th2d, phid is ON or OFF
        """
   
        log_info(self, "In is_corr_on")
        return(self.dcd_corr)


    # ---------- dcd_corr_show() -------------
    @object_method
    def dcd_corr_show(self,_ = None):
        """
         Show correction tables. They concern correction of
         positions of th1d, th2d and phid for a given mu range.
        """
   
        log_info(self, "In dcd_corr_show")
       
        nblines = len(self.corr_values_sorted) 
        if nblines == 0:
            print("No TH1D,TH2D and PHID correction values loaded !!")
        else:
            print("TH1D,TH2D and PHID CORRECTION VALUES (flag: -1/0/1 = no change/absolute/relative pos.)")
            print("<mu_min> <mu_max> <th1d_pos> <th1d_flag> <th2d_pos> <th2d_flag> <phid_pos> <phid_flag>")
            for ival in range(nblines):
                print("%7.3f  %7.3f   %7.5f    %5d       %7.5f    %5d       %7.5f    %5d" % \
                (self.corr_values_sorted[ival][0], self.corr_values_sorted[ival][1], \
                self.corr_values_sorted[ival][2], self.corr_values_sorted[ival][3], \
                self.corr_values_sorted[ival][4], self.corr_values_sorted[ival][5], \
                self.corr_values_sorted[ival][6], self.corr_values_sorted[ival][7]))
        


    # ---------- dcd_show() -------------
    def __info__(self):
        """
         Gives different useful info
        """
        info_list = []
        info_list.append("Double Crystal Deflector")
        info_list.append("------------------------")
        
        info_list.append(f"  Distance 1st crystal-sample = {self.dcd_l}")
        info_list.append(f"  CC Monochromator energy     = {self.energy_ccmono.position:f} keV")
        info_list.append(f"  Max mu angle (th30)         = {self.dcd_th30:f}")
        # Temporarily print next 2 for info independently of their flags
        #print("Rhod offset                 = %f" % self.rhod_offset)
        #print("Delta offset                = %f" % self.delta_offset)

        if self._dcd_on == True:
            info_list.append("  Coupling mu <-> real motors = ON")
            info_list.append(f"  Rhod offset                 = {self.rhod_offset}")
            if self.dcd_delflag == True:
                info_list.append(f"  Delta offset                = {self.delta_offset:f}")
                info_list.append(f"  Theta offset                = {self.theta_offset:f}")
        else:
            info_list.append("  Coupling mu <-> real motors = OFF")

        flag = "ON" if self.dcd_delflag else "OFF"
        info_list.append(f"  Usage of delta in coupling  = {flag}")

        flag = "ON" if self.dcd_thetaflag else "OFF"
        info_list.append(f"  Usage of theta in coupling  = {flag}")

        if self.dcd_corr == True:
            info_list.append("  th1d, th2d, phid correction = ON")
            info_list.append(f"  path to correction table file = {self.dcd_corr_path}")
            info_list.append(f"  correction table file = {self.dcd_corr_file}")
        else:
            info_list.append("  th1d, th2d, phid correction = OFF")

        
        return "\n".join(info_list) + "\n\n"


    # ---------- dcd_set_th30() -------------
    @object_method(types_info=("float", "None"))
    def dcd_set_th30(self,_,value):
        """
        Set maximum deflection angle 'manually'
        Remark: Second arg (= _) indicates axis
        """ 
        print("Will set new th30 value")
        self.dcd_th30 = value
        print("Value = %f" % value)
        # update it in settings
        self._dcd_th30.set(self.dcd_th30)
        # recalculate offsets, since dcd_th30 is used in the 
        # formulas for calculating offsets
        self.dcd_calc_offsets()


    @property
    def mu_to_set(self):
        return self._mu_to_set.get()

    @mu_to_set.setter
    def mu_to_set(self, value):
        self._mu_to_set.set(value)
