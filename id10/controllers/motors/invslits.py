# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) 2016 Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

from bliss.common.logtools import log_debug, log_info
from bliss.controllers.motor import CalcController

"""
example for single VERTICAL slits:
  \     UP      /
   \___________/
                     VGAP
    ___________      VOFF
   /           \
  /    DOWN     \

-
  controller:
    class: invslits
    package: id10.controllers.motors.invslits
    slit_type: vertical
    axes:
        -
            name: $sgvg
            tags: real vgap
        -
            name: $sgvo
            tags: real voffset
        -
            name: sgu 
            tags: up
        -
            name: sgd
            tags: down
"""

"""
<slit_type> : [horizontal | vertical | both]
              default value : both
"""

class Invslits(CalcController):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)


    def initialize_axis(self, axis):
        CalcController.initialize_axis(self, axis)
        axis.no_offset = True


    def calc_to_real(self, positions_dict):
        log_info(self, "[SLITS] calc_to_real()")
        log_info(self, "[SLITS]\treal: %s" % positions_dict)

        real_dict = dict()
        slit_type = self.config.get("slit_type", default="both")

        if slit_type not in ['vertical']:
            real_dict.update(
                { "hoffset":
                  (positions_dict["back"] - positions_dict["front"]) / 2.0,
                  "hgap":
                  positions_dict["back"] + positions_dict["front"]
                  } )

        if slit_type not in ['horizontal']:
            real_dict.update(
                { "voffset":
                  (positions_dict["up"] - positions_dict["down"]) / 2.0,
                  "vgap":
                  positions_dict["up"] + positions_dict["down"]
                  } )

        log_info(self, "[SLITS]\treal: %s" % real_dict)

        return real_dict


    def calc_from_real(self, positions_dict):
        log_info(self, "[SLITS] calc_from_real()")
        log_info(self, "[SLITS]\tcalc: %s" % positions_dict)

        calc_dict = dict()
        slit_type = self.config.get("slit_type", default="both")

        if slit_type not in ['vertical']:
            calc_dict.update(
                { "back":
                  (positions_dict["hgap"] / 2.0) + positions_dict["hoffset"],
                  "front":
                  (positions_dict["hgap"] / 2.0) - positions_dict["hoffset"]
                  } )

        if slit_type not in ['horizontal']:
            calc_dict.update(
                { "up":
                  (positions_dict["vgap"] / 2.0) + positions_dict["voffset"],
                  "down":
                  (positions_dict["vgap"] / 2.0) - positions_dict["voffset"]
                  } )

        log_info(self, "[SLITS]\tcalc: %s" % calc_dict)

        return calc_dict
