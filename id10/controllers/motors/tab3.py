"""
ID10 3-leg Table support

On ID10 there are 4 3-leg tables.

1 is in EH1 to support double-crystal deflector
2 are in OH1 to support mirrors 
and 1 is in EH2 to support ...

Orientation of these 3-leg tables are in the scetch below
which is a top view i.e. vertical Z-axis is pointing out
of the plane towards observer:


In EH1:
-------

Geometry 3 is used, where the calculated z motor is representing
z value of the point B, which is mid-point between 2 'back' legs 
[zurd, zufd] 
The 2 'back' legs are tagged as 'back1' and 'back2'.
The remaining leg: zdd is tagged as 'front' motor.


               zurd
                /   ^
              / |   |
            /   |   |
          /     |   |
        /       |   |
    zdd---------B-- d1 ---<---- X-ray beam (X-axis)
        \       |   |
          \     |   |
            \   |   |
              \ |   |
                \   v
               zufd 
       <--d2 -->|
                |
                |
	        v
             Y-axis

2 other calculated motors represent tilt along X (= rotation
around Y in point B) and tilt along Y (= rotation around X in 
point B)


In OH1:
-------

Geometry 3 is used, where the calculated z motor is representing
z value of the point B, which is mid-point between 2 'back' legs 
[leg1dr, leg1df] for mirror1 and
[leg2ur, leg2uf] for mirror2
The 2 'back' legs of each mirror are tagged as 'back1' and 'back2'.
The remaining leg: leg1u and leg2d is tagged as 'front' motor.

         mirror2                  mirror1

              leg2ur             leg1dr
                /   ^           ^   \           
              / |   |           |   | \
            /   |   |           |   |   \
          /     |   |           |   |     \
        /       |   |           |   |       \
  leg2d---------B-- d1 ---------d1--B---------leg1u---<---- X-ray beam
        \       |   |           |   |       /               (X-axis)
          \     |   |           |   |     /
            \   |   |           |   |   /
              \ |   |           |   | /
                \   v           v   /
              leg2uf              leg1df
       <-- d2-->|                   |<-- d2 -->
                |                   |
	        v                   v
              Y-axis              Y-axis

2 other calculated motors represent tilt along X (= rotation
around Y in point B) and tilt along Y (= rotation around X in 
point B)

In EH2:
-------

Geometry 0 is used, where the calculated z motor is representing
z value of the point C (mid-point between 'front' leg [jdd] and 
the mid-point B between 2 'back' legs [jdur and jdul]
The 2 'back' legs are tagged as 'back1' and 'back2'.
The leg jdd is tagged as 'front' motor.

               jdur
                /   ^
              / |   |
            /   |   |
          /     |   |
        /       |   |
    jdd-----C---B-- d1 ---<---- X-ray beam (X-axis)
        \   |   |   |
          \ |   |   |
            \   |   |
            | \ |   |
            |   \   v
            |  jdul 
       <--d2|-->
            |
            |
	    v
          Y-axis

2 other calculated motors represent tilt along X (= rotation
around Y in point C) and tilt along Y (= rotation around X in 
point C)
       
Eample of of configuration file (tab3_eh2.yml) from ID10:
---------------------------------------------------------

controller:
- class: tab3
  package: id10.controllers.motors.tab3
  geometry: 0
  d1: 905
  d2: 722
  axes:
  - name: $jdd
    tags: real front
  - name: $jdur
    tags: real back1
  - name: $jdul
    tags: real back2
  - name: jdtiltx
    tags: xtilt
    low_limit: -30.0000
    high_limit: 30.0000
    unit: mrad
  - name: jdtilty
    tags: ytilt
    low_limit: -178.6065
    high_limit: 181.3935
    unit: mrad
  - name: jdz
    tags: z
    low_limit: -55.0000
    high_limit: 60.0000
    tango_server: eh2_exp


Remark: Possible unit for angles (xtilt and ytilt) is:
        rad  ... radian
        mrad ... milliradian
        deg  ... degree
"""

from numpy import *

from bliss.common.logtools import log_debug, log_info
from bliss.controllers.motor import CalcController

class tab3(CalcController):
    def initialize(self):
        CalcController.initialize(self)

        log_info(self, "In tab3 constructor")

        self.geometry = self.config.get("geometry", int)
        self.d1 = self.config.get("d1", float)
        self.d2 = self.config.get("d2", float)
        log_debug(self, "geometry = %d" % self.geometry)
        log_debug(self, "d1 = %f" % self.d1)
        log_debug(self, "d2 = %f" % self.d2)


    def initialize_axis(self, axis):
        CalcController.initialize_axis(self, axis)


    def calc_from_real(self, pos):

        log_info(self, "calc_from_real")

        front, back1, back2 = pos['front'], pos['back1'], pos['back2']
        log_debug(self, "front position = %f" % front)
        log_debug(self, "back1 position = %f" % back1)
        log_debug(self, "back2 position = %f" % back2)
      
        xtiltaxis = self._tagged["xtilt"][0]
        xtiltaxis_unit = xtiltaxis.config.get("unit")
        ytiltaxis = self._tagged["ytilt"][0]
        ytiltaxis_unit = ytiltaxis.config.get("unit")

        # xtauf = xtilt angular unit factor 
        if xtiltaxis_unit == "rad":
            xtauf = 1
        elif xtiltaxis_unit == "mrad":
            xtauf = 1000
        elif xtiltaxis_unit == "deg":
            xtauf = 180/pi
        else:
            raise ValueError("Unknown xtilt angular unit %s (should be rad, mrad or deg)" % xtiltaxis_unit)

        # ytauf = ytilt angular unit factor 
        if ytiltaxis_unit == "rad":
            ytauf = 1
        elif ytiltaxis_unit == "mrad":
            ytauf = 1000
        elif ytiltaxis_unit == "deg":
            ytauf = 180/pi
        else:
            raise ValueError("Unknown ytilt angular unit %s (should be rad, mrad or deg)" % ytiltaxis_unit)

        if self.geometry == 0:

            xtilt = xtauf * arctan((back2 - back1) / self.d1)
            ytilt = ytauf * arctan(((back2 + back1) / 2 - front) / self.d2)
            z = (back1 + back2 + 2 * front) / 4

        elif self.geometry == 3:
            xtilt = xtauf * arctan((back2 - back1) / self.d1)
            ytilt = ytauf * arctan(((back2 + back1) / 2 - front) / self.d2)
            z = (back1 + back2) / 2 

        else:
            raise ValueError("Only geometry 0 and 3 are covered with this controller")
        return {"xtilt": xtilt, "ytilt": ytilt, "z": z}


    def calc_to_real(self, pos):

        log_info(self, "calc_from_real")

        xtilt, ytilt, z = pos['xtilt'], pos['ytilt'], pos['z']

        log_debug(self, f"xtilt position = {xtilt}")
        log_debug(self, f"ytilt position =  {ytilt}")
        log_debug(self, f"z position = {z}")

        xtiltaxis = self._tagged["xtilt"][0]
        xtiltaxis_unit = xtiltaxis.config.get("unit")
        ytiltaxis = self._tagged["ytilt"][0]
        ytiltaxis_unit = ytiltaxis.config.get("unit")

        # xtauf = xtilt angular unit factor 
        if xtiltaxis_unit == "rad":
            xtauf = 1
        elif xtiltaxis_unit == "mrad":
            xtauf = 1/1000.
        elif xtiltaxis_unit == "deg":
            xtauf = pi/180.
        else:
            raise ValueError("Unknown xtilt angular unit %s (should be rad, mrad or deg)" % xtiltaxis_unit)

        # ytauf = ytilt angular unit factor 
        if ytiltaxis_unit == "rad":
            ytauf = 1
        elif ytiltaxis_unit == "mrad":
            ytauf = 1/1000.
        elif ytiltaxis_unit == "deg":
            ytauf = pi/180.
        else:
            raise ValueError("Unknown ytilt angular unit %s (should be rad, mrad or deg)" % ytiltaxis_unit)

        tgx = tan(xtauf * xtilt)
        tgy = tan(ytauf * ytilt)

        if self.geometry == 0:
            back1 = z + ((self.d2 * tgy - self.d1 * tgx) / 2)
            back2 = z + ((self.d2 * tgy + self.d1 * tgx) / 2)
            front = z - (self.d2 * tgy / 2)
        elif self.geometry == 3:
            back1 = z - (self.d1 * tgx / 2)
            back2 = z + (self.d1 * tgx / 2)
            front = z - (self.d2 * tgy)
        else:
            raise ValueError("Only geometry 0 and 3 are covered with this controller")
        return {"back1": back1, "back2": back2, "front": front}
