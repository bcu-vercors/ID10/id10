"""
On the flight-Path setup the tab4 marble is supporting a 2-translation setup
so-called the assembly, a kind of horizontal tab2 but with an tilt angle against the Y axis.

     in which case the 2 supporting translations:
     transup (upstream) and transdown (downstream)
     are moving in horizontal (= Y-axis) direction.
     The 2 REAL motors are on the scetch below marked as transup and transdown.
     The 2 CALCULATED motors are:
     - motor for the horizontal translation (ttrans) of the
       "center" (= point marked C on the scetch below)
     - motor for the rotation (trot) around Z-axis going trough C.
     The distance between C and upstream trans (transup) is d1 and
     the distance between "center" and downstream trans (transdown) is d2 and
     the angle against Y-axis is angle:


      _____________________
      |                    |     
      |\           \       |     
      | \           \ angle|     
      |  \           \     |     
      |   \ <d2>C<d1> \    |----<------ X-axis (along X-ray beam)     
      |    \  Z-axis   \   |     
      |     \   |       \  |     
      |      \  |        \ |     
      |       v |         v|     
      |_________|__________|
      transdown v         transup
                Y-axis   
       

   Remark:
   For ID10 assembly table the 2 physical
   motors used are: ytu and ytd from which we derive the
   calculated motors tab4y and tab4rot.
   ytu and yrd represent motors of 2 translation of the table and
   tab4y and tab4rot are calculated motors
   --> so we have the following motors:
       transup   = ytu   ... real motor
       transdown = ytd   ... real motor
       ttrans  = tab4y    ... calculated motor
       trot    = tab4rot   ... calculated motor


Configuration parameters:

* d1: distance (along X-axis) between "center" of the table and 
      upstream translation 
* d2: distance (along X-axis) between "center" of the table and
      downstream translation 
* transup: alias for real upstream axis
* transdown: alias for real downstream axis
* ttrans: alias for Y-axis
          translation calculated axis
* trot: alias for Z-axis 
        rotation calculated axis

Example of configuration file (assembly.yml) from ID10:
---------------------------------------------------

# Here is the config for 2-translation support table:
#
controller:
- class: Assembly 
  package: id10.controllers.motors.assembly
  d1: 821.
  d2: 1621.
  angle: 5.0  # in degree only
  axes:
  - name: $ytu
    tags: real transup
  - name: $ytd
    tags: real transdown
  - name: tab4y
    tags: ttrans
  - name: tab4rot
    tags: trot
    unit: mrad

Remark: Possible unit for angle (trot) is:
        rad  ... radian
        mrad ... milliradian
        deg  ... degree

"""
from bliss.common.logtools import log_debug, log_info
from bliss.controllers.motor import CalcController
import numpy as np


class assembly(CalcController):
    def initialize(self):
        CalcController.initialize(self)
        self.d1 = self.config.get("d1", float)
        self.d2 = self.config.get("d2", float)
        angle = self.config.get("angle", float, 0)
        self.angle = np.radians(angle)
        self.trot_unit = "mrad"

        log_debug(self, f"d1 = {self.d1}")
        log_debug(self, f"d2 = {self.d2}")
        log_debug(self, f"angle = {angle}")
        
    def initialize_axis(self, axis):
        if axis.config.get("tags") == "trot":
            trot_unit = axis.config.get("unit", str, None)
            if trot_unit is None:
                axis.config.set("unit", self.trot_unit)
            else:
                self.trot_unit = trot_unit

        # auf = angular unit factor
        if self.trot_unit == "rad":
            self.auf = 1
        elif self.trot_unit == "mrad":
            self.auf = 1000
        elif self.trot_unit == "deg":
            self.auf = np.degrees(1)
        else:
            raise ValueError("Unknown angular unit %s (should be rad, mrad or deg)" % rotaxis_unit)
        
    def calc_from_real(self, positions_dict):
        log_info(self, "calc_from_real()")
        auf = self.auf
        transup = positions_dict["transup"]
        transdown = positions_dict["transdown"]

        trot = self.assembly_rot(transup, transdown, self.d1, self.d2, self.angle) * auf
        ttrans = self.assembly_offset(transup, transdown, self.d1, self.d2, self.angle)
        
        #ttrans = (self.d1 * (legup-legdown) / (self.d1 + self.d2)) + legdown
        #trot = np.arctan((legup - legdown)/(self.d1 + self.d2)) * auf
        return {"ttrans": ttrans, "trot": trot}


    def calc_to_real(self, positions_dict):
        log_info(self, "calc_to_real()")
        auf = self.auf
        
        ttrans = positions_dict["ttrans"]
        trot = positions_dict["trot"]

        transup = self.assembly_trans(trot/auf,ttrans, self.d1, self.angle)
        transdown = self.assembly_trans(trot/auf,ttrans, self.d2, self.angle)

        
        #legdown = ttrans - self.d1 * np.tan(trot * auf)
        #legup = ttrans + self.d2 * np.tan(trot * auf)

        return {"transup": transup, "transdown": transdown}


    def assembly_trans(self, trot, ttrans, distance, angle):
        s = np.cos (self.angle) + np.sin(angle) * np.tan(trot)
        if s.any() != 0:
            return (ttrans + distance * np.tan (trot)) / s
        else:    
            raise ValueError("Divide by 0 in as_trans in assembly")

    def assembly_rot(self, transup, transdown, d1, d2, angle):
        dy = (transup-transdown) * np.cos(angle)
        dx = (d1-d2) - (transup-transdown) * np.sin(angle)
        return np.arctan(dy/dx)

    def assembly_offset(self, transup, transdown, d1, d2, angle):
        return (d1 * transdown - d2 * transup) * np.cos(angle)/ \
            (d1 - d2 - (transup - transdown) * np.sin(angle))
