"""
ID10 pseudo motor which position is the arithmetic sum 
of the positions of 2 real motors

Configuration parameters:

* mot1: alias for 1st real axis
* mot2: alias for 2nd real axis
* motmean: alias for calculated axis

Example of configuration file (motmean.yml) from ID10:
------------------------------------------------------

# Here is the config for arithmetic mean motor:
#
controller:
- class: motmean
  package: id10.controllers.motors.motmean
  axes:
  - name: $wbdmzu
    tags: real mot1
  - name: $wbdmzd
    tags: real mot2
  - name: wmdmz
    tags: motmean

"""

from bliss.controllers.motor import CalcController
import math


class motmean(CalcController):
    def __init__(self, *args, **kwargs):
        CalcController.__init__(self, *args, **kwargs)

    def calc_from_real(self, positions_dict):
        mot1 = positions_dict["mot1"]
        mot2 = positions_dict["mot2"]

        return {
            "motmean": (mot1 + mot2)/2.
        }

    def calc_to_real(self, positions_dict):
        motmean = positions_dict["motmean"]

        m1 = self._tagged["mot1"][0].position
        m2 = self._tagged["mot2"][0].position

        deltamean = motmean - (m1 + m2) / 2. 
        mot1 = m1 + deltamean
        mot2 = m2 + deltamean
        return {
            "mot1": mot1,
            "mot2": mot2
        }
