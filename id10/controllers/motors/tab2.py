"""
ID10 2-leg Table support

On ID10 there are 2 types of 2-leg tables.

A) First one (used as a part of White Beam Transfocator support) has:
   - longer dimension along X-axis (= X-ray beam axis)
   - shorter dimension along Y-axis 
     in which case the 2 supporting legs:
     legup (upstream) and legdown (downstream)
     are moving in vertical (= Z-axis) direction.
     The 2 REAL motors are on the scetch below marked as legup and legdown.
     The 2 CALCULATED motors are:
     - motor for the vertical translation (ttrans) of the
       "center" (= point marked C on the scetch below)
     - motor for the rotation (trot) around Y-axis going trough C.
     The distance between C and upstream leg (legup) is d1 and
     the distance between "center" and downstream leg (legdown) is d2:

               Z-axis
		 ^
		 |
                 |
          _______|______________
         /       |            /
        /        |           /
       / .<--d2->C<--d1-->.-/----<------ X-axis (along X-ray beam)
      /  |      /         |/
     /   |     /          |
    /____|____/__________/|       
         |   /            |
         |  v Y-axis      | 
         |                |
        legdown          legup

   Remark:
   For ID10 White-Beam Transfocator support table the 2 physical
   motors used are: tfzu (= legup) and tfzd (= legdown) from which
   we derive the calculated motors tfz (= ttrans) and tfry (= trot).
   tfzu and tfzd represent motors of 2 legs of the table and
   tfry and tfz are calculated motors (derived from d1,d2 and tfzu,tfzd)
   --> so we have the following motors:
       legup   = tfzu   ... real motor
       legdown = tfzd   ... real motor
       ttrans  = tfz    ... calculated motor
       trot    = tfry   ... calculated motor


B) Second one (used as a part of White-Beam Double Mirror? support) has:
   - longer dimension along Y-axis
   - shorter dimension along X-axis (= X-ray beam axis) 
     in which case the 2 supporting legs:
     legup (upstream) and legdown (downstream)
     are moving in horizontal (= Y-axis) direction.
     The 2 REAL motors are on the scetch below marked as legup and legdown.
     The 2 CALCULATED motors are:
     - motor for the horizontal translation (ttrans) along Y-axis
       of the "center" (= point marked C on the scetch below)
     - motor for the rotation (trot) around Z-axis going trough C.
     The distance between C and upstream leg (legup) is d1 and
     the distance between "center" and downstream leg (legdown) is d2:

                Z-axis
 		 ^
             _____|____________
            /     |           /
           /      |          /
          /       |         /
         /        |        /
        / .<--d2->C<-d1->./----<------ X-axis (along X-ray beam)
       / /       /      //
      / /       /      //
     /_/______ /_____ //
      /       /      /
     /       v      /  
    v     Y-axis   v
   legdown        legup

   Remark:
   For ID10 White-Beam Double Mirror support table the 2 physical
   motors used are: wbdmyu and wbdmyd from which we derive the
   calculated motors wbdmy and wbdmrz.
   wbdmyu and wbdmyd represent motors of 2 legs of the table and
   wbdmy and wbdmrz are calculated motors (derived from d1,d2 and 
   wbdmyu, wbdmyd)
   --> so we have the following motors:
       legup   = wbdmyu   ... real motor
       legdown = wbdmyd   ... real motor
       ttrans  = wbdmy    ... calculated motor
       trot    = wbdmrz   ... calculated motor


Configuration parameters:

* d1: distance (along X-axis) between "center" of the table and 
      upstream leg 
* d2: distance (along X-axis) between "center" of the table and
      downstream leg 
* legup: alias for real upstream leg axis
* legdown: alias for real downstream leg axis
* ttrans: alias for Z-axis (type A) or Y-axis (type B) 
          translation calculated axis
* trot: alias for Y-axis (type A) or Z-axis (type B) 
        rotation calculated axis

Example of configuration file (tab2.yml) from ID10:
---------------------------------------------------

# Here is the config for 2 2-leg support tables:
# one for transfocator and one for white-beam double mirror
#
controller:
- class: tab2 
  package: id10.controllers.motors.tab2
  d1: 550.
  d2: 550.
  axes:
  - name: $tfzu
    tags: real legup
  - name: $tfzd
    tags: real legdown
  - name: tfz
    tags: ttrans
  - name: tfry
    tags: trot
    unit: mrad

- class: tab2 
  package: id10.controllers.motors.tab2
  d1: 980.
  d2: 380.
  axes:
  - name: $wbdmyu
    tags: real legup
  - name: $wbdmyd
    tags: real legdown
  - name: wbdmy
    tags: ttrans
  - name: wbdmrz
    tags: trot
    unit: mrad


Remark: Possible unit for angles (trot) is:
        rad  ... radian
        mrad ... milliradian
        deg  ... degree

"""
from bliss.common.logtools import log_debug, log_info
from bliss.controllers.motor import CalcController
import numpy as np


class tab2(CalcController):
    def initialize(self):
        CalcController.initialize(self)
    
        self.d1 = self.config.get("d1", float)
        self.d2 = self.config.get("d2", float)
        self.trot_unit = "mrad"
        log_debug(self, "d1 = %f" % self.d1)
        log_debug(self, "d2 = %f" % self.d2)

    def initialize_axis(self, axis):
        if axis.config.get("tags") == "trot":
            trot_unit = axis.config.get("unit", str, None)
            if trot_unit is None:
                axis.config.set("unit", self.trot_unit)
            else:
                self.trot_unit = trot_unit

        # auf = angular unit factor
        if self.trot_unit == "rad":
            self.auf = 1
        elif self.trot_unit == "mrad":
            self.auf = 1000
        elif self.trot_unit == "deg":
            self.auf = np.degrees(1)
        else:
            raise ValueError("Unknown angular unit %s (should be rad, mrad or deg)" % rotaxis_unit)
        
    def calc_from_real(self, positions_dict):

        log_info(self, "calc_from_real()")
        auf = self.auf
        
        legup = positions_dict["legup"]
        legdown = positions_dict["legdown"]

        rotaxis = self._tagged["trot"][0]
        rotaxis_unit = rotaxis.config.get("unit")
        log_debug(self, "rotational axis unit = %s" % rotaxis_unit)


        ttrans = (self.d1 * (legup-legdown) / (self.d1 + self.d2)) + legdown
        trot = np.arctan((legdown - legup)/(self.d1 + self.d2)) * auf
        return {"ttrans": ttrans, "trot": trot}


    def calc_to_real(self, positions_dict):

        log_info(self, "calc_to_real()")
        auf = self.auf
        ttrans = positions_dict["ttrans"]
        trot = positions_dict["trot"]

        legdown = ttrans + self.d1 * np.tan(trot / auf)
        legup = ttrans - self.d2 * np.tan(trot / auf)

        return {"legup": legup, "legdown": legdown}
