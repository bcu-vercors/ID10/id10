
# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) 2016 Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

"""
Class where by moving "calculational" motor q45 two physical 
motors ("ydet" and "zdet") are moved. So 2D scan with "ydet"
and "zdet" can be replaced by 1D scan on q45.

    zdet
      ^            q45
      |            /
      |          /
      |        /
      |      /
      |    /  
      |  /
      |/
      -----------------> ydet

Real motor roles:
* *ydet*: translation of detector along Y-axis
* *zdet*: translation of detector along Z-axis

Calculated motor roles:
* *q45*: "diagonal" move (move of scattering vector)

Configuration parameters
* *samptodet*: sample-to-detector distance [mm]
* *lam*: X-ray wavelength [angstroem]
* *coupling*: flag indicating if coupling of ydet and zdet to 
              q45 and q45 offset is ON (value = True) or 
                                   OFF (value = False)

Example configuration::

controller:
  class: q45coup
  package: id10.controllers.motors.q45coup
  name: q45coup
  description: EH2 detector simultaneous translation in Y and Z
  samptodet: 5330.0  
  lam: 1.531
  coupling: True 
  axes:
  - name: $ydet
    tags: real ydet
  - name: $zdet
    tags: real zdet
  - name: q45
    description: scattering vector at 45 degrees 
    tags: q45
    #low_limit: 0.0
    #high_limit: 1.45
    user_tag:
    - EH2.Q45
"""

from bliss.config import settings
from bliss.common.logtools import log_debug, log_info
from bliss.controllers.motor import CalcController
import math


class q45coup(CalcController):

    def __init__(self, *args, **kwargs):
        super(q45coup, self).__init__(*args, **kwargs)

        hash_name = 'id10.q45'
        ctrl_name = self.config.get('name', default=None)
        if ctrl_name:
            hash_name += '.%s' % ctrl_name    

        log_info(self, "In q45coup constructor")
        log_debug(self, "ctrl_name = %s" % ctrl_name)
        log_debug(self, "hash_name = %s" % hash_name)

        self._samptodet = settings.SimpleSetting(hash_name + '.samptodet')
        log_debug(self, "samptodet from settings = %r" % self._samptodet.get())
        if self._samptodet.get() == None:
            log_debug(self, "samptodet does not exist in settings")
            log_debug(self, "Try to get it from config")
            self._samptodet = self.config.get('samptodet')
            if self._samptodet == None:
                log_debug(self, "samptodet does not exist in config")
                return
            else:
                log_debug(self, "samptodet from config = %r" % self._samptodet.get())
   
        self._lam = settings.SimpleSetting(hash_name + '.lam')
        log_debug(self, "lam from settings = %r" % self._lam.get())
        if self._lam.get() == None:
            log_debug(self, "lam does not exist in settings")
            log_debug(self, "Try to get it from config")
            self._lam = self.config.get('lam')
            if self._lam == None:
                log_debug(self, "lam does not exist in config")
                return
            else:
                log_debug(self, "lam from config = %r" % self._lam.get())


        self._coupling = settings.SimpleSetting(hash_name + '.coupling', default_value=True)
        log_debug(self, "coupling from settings = %r" % self._coupling.get())
        if self._coupling.get() == None:
            log_debug(self, "coupling does not exist in settings")
            log_debug(self, "Try to get it from config")
            self._coupling = self.config.get('coupling')
            if self._coupling == None:
                log_debug(self, "coupling does not exist in config")
                return
            else:
                log_debug(self, "coupling from config = %r" % self._coupling.get())

            
    def initialize(self):
        super().initialize()
        log_info(self, "In initialize")
	# Get initial position for 'ydet' and 'zdet' motors
        self.ydet_offset = self.ydet.position
        self.zdet_offset = self.zdet.position
        log_debug(self, "Initial ydet position = %f" % self.ydet_offset)
        log_debug(self, "Initial zdet position = %f" % self.zdet_offset)


    @property
    def samptodet(self):
        return self._samptodet.get()

    @samptodet.setter
    def samptodet(self, value):
        self._samptodet.set(value)

    @property
    def lam(self):
        return self._lam.get()

    @lam.setter
    def lam(self, value):
        self._lam.set(value)


    @property
    def ydet(self):
        return self._tagged['ydet'][0]

    @property
    def zdet(self):
        return self._tagged['zdet'][0]


    @property
    def coupling(self):
        return self._coupling.get()

    @coupling.setter
    def coupling(self, value):
        self._coupling.set(value)


    def calc_from_real(self, positions_dict):
        '''
        Returns calculated/virtual motor "q45" position from real ones
        (either "ydet" or "zdet").
	Remark: Normally will NEVER derive calculated/virtual motor "q45", 
		but will rather move it and as a consequence the 2 
	 	real motors ("ydet" and "zdet") will be moved.

        Units:
            Distance samptodet = in mm
            Ydet - Ydet_offset = in mm
            Zdet - Zdet_offset = in mm
            X-ray wavelength (lam) = in angstroems 
	    Calculated/virtual motor (q45) = in angstroems^(-1).
        '''

        log_info(self, "In q45coup calc_from_real")

        if self._coupling.get() == True:
            log_debug(self, "calc_from_real(): coupling is True")
            log_debug(self, "calc_from_real():        ydet = %f" % positions_dict["ydet"])
            log_debug(self, "calc_from_real(): ydet_offset = %f" % self.ydet_offset)
            log_debug(self, "calc_from_real():        zdet = %f" % positions_dict["zdet"])
            log_debug(self, "calc_from_real(): zdet_offset = %f" % self.zdet_offset)

            q45_position = (positions_dict["ydet"] - self.ydet_offset) * \
	     2*math.sqrt(2.)*math.pi / (self._samptodet.get() * self._lam.get())
	    # Remark: could use similar formula to get q45 out of zdet and its
            #         offset

            log_debug(self, "calc_from_real(): q45 position = %f" % q45_position)
            return dict(q45=q45_position)
        else:
            log_debug(self, "calc_from_real(): coupling is False")
            return {}


    def calc_to_real(self, positions_dict):
        '''
	When coupling is active:
        Returns real motors positions of motors "ydet" and "zdet" (as a 
        dictionary) given the position of the virtual motor "q45".
        Units:
            Sample to Detector distance (samptodet) = in mm
            X-ray wavelength (lam) = in angstroems 
            Ydet - Ydet_offset     = in mm
            Zdet - Zdet_offset     = in mm
	    Calculated/virtual motor (q45) position = in angstroems^(-1)

        '''

        log_info(self, "In q45coup calc_to_real")

        if self._coupling.get() == True:
            log_debug(self, "calc_to_real(): coupling is True")
            q45_position = positions_dict["q45"]
            log_debug(self, f"calc_to_real(): q45 position {q45_position}")
            log_debug(self, "calc_to_real(): ydet_offset = %f" % self.ydet_offset)
            log_debug(self, "calc_to_real(): zdet_offset = %f" % self.zdet_offset)

            ydet_position = self.ydet_offset + (1./math.sqrt(2.)) * \
            self._samptodet.get() * (self._lam.get()/(2*math.pi)) * q45_position
            log_debug(self, f"calc_to_real(): ydet position = {ydet_position}")
            zdet_position = self.zdet_offset + (1./math.sqrt(2.)) * \
            self._samptodet.get() * (self._lam.get()/(2*math.pi)) * q45_position
            log_debug(self, f"calc_to_real(): zdet position = {zdet_position}")
            return dict(ydet=ydet_position,zdet=zdet_position)
        else:
            log_debug(self, "calc_to_real(): coupling is False")
            return {}


    def __info__(self):
        ctrl_name = self.config.get('name', default=None)
        name = 'Q45' + ('' if ctrl_name is None else ' ' + ctrl_name)
        try:
            return "{name}:\n" \
               "  .samptodet = {0.samptodet} mm\n" \
               "  .lam = {0.lam} angstrom\n" \
               "  .coupling = {0.coupling}\n".format(self, name=name)
        except AttributeError: # in __init__ not yet fully instantiated
            return super().__info__()

