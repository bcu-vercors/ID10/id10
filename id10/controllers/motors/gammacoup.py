# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) 2016 Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

"""
Class to use for the detector support translation AND rotation
so that A DETETOR ALWAYS POINTS TO A SAMPLE. 

Since the center of rotation is not at the sample position,
need also to translate.

A move is achieved by using the calculated motor 'gamma',
which has the same (angle) value as real motor 'r3'.

When calculated motor 'gamma' makes a rotation, the real
motor 'r3' make a rotation for the same amount, while the
motor 'z3' makes a necessary translation, such that 
A DETETOR ALWAYS POINTS TO A SAMPLE (see sketch below).

  Detector
       ____
           ____
               ____
                   ____
                       ____ 
                           ----
		R3    gamma   ---- 
    _____________x__________ --------- Sample   <-------------- BEAM
              |\   /|
	      | \_/ |
	      |     |
	      |  |<-|---- samptor3 ----->|
	      |     |
	      |     |
	      |  ^  |
	      |  |  |
              |  |  |
	      | z3  |
	      |  |  |
	      |  |  |
	      |  v  | 
	      |     |
	      -------


Real motor roles:
* *r3*: rotation of detector support around R3 center of rotation
* *z3*: translation of detector support along Z (vertical) axis.

Calculated motor roles:
* *gamma*: angle equal to r3 [degrees]

Configuration parameters:
* *samptor3*: sample-to-R3 axis distance [mm]

Example configuration file (gammacoup.yml) from id10:
-----------------------------------------------------

controller:
  class: gammacoup
  package: id10.controllers.motors.gammacoup
  description: EH2 detector translation and rotation
  samptor3: 700. 
  axes:
  - name: $r3
    tags: real r3
  - name: $z3
    tags: real z3
  - name: gamma
    description: angle of detector support with respect to horizontal line
    tags: gamma
    # put the same limits as found for r3
    low_limit: -13.58545
    high_limit: 30.61455
    user_tag:
    - EH2.GAMMA

-----------------------------------------------------


The BLISS session which wants to use this controller should normally
load the script 'gamma_coupling_onoff.py' in its setup file and should
not put motors: 'r3', 'z3' and 'gamma' in the list of
config-objects in its yml file, but rather rely on the dynamic
inclusion/exclusion of different motor objects by invoking in the
BLISS shell the functions:
gamma_coupling_on() ... only the calculated motor 'gamma' is visible in
                        the BLISS shell and can be moved. As a consequence
                        of its move, the 2 real motors: 'r3', 'z3'
                        are moved in a coupled/synchronous way.
gamma_coupling_off() ... only 2 physical motors: 'r3', 'z3'
                         are visible in the BLISS shell and can be moved
                         independently. There is no more coupling among them.
The last used situation (coupling ON or OFF) is stored in the settings
(= in the Redis DB) and can be checked by typing in the BLISS shell:
is_gamma_coupling_on() ... invoking this funcion tells if the coupling of
                           the 2 real motors 'r3' and 'z3' to the calculated
                           motor 'gamma' is ON (answer = True) or
                                           OFF (answer = False).
The script 'gamma_coupling_onoff.py' is in the 'standard' place:
~blissadm:local/beamline_configuration/sessions/scripts

Example session (testgc) configuration file (testgc.yml) from id10:
-------------------------------------------------------------------
class: Session
name: testgc
setup-file: ./testgc.py
config-objects: []


Example session (testgc) setup file (testgc.py) from id10:
----------------------------------------------------------
print("Wellcome to testgc session setup file")
load_script('gamma_coupling_onoff')

Even when coupling is ON and therefore 2 physical motors:
'r3', 'z3' are not directly accesible, one can
reach them at controller level. For ex. if want to see the
'z3' position, we type on the BLISS shell:
gamma.controller.z3.position

Since sample to r3 center detector distance can change,
after each measurement of this distance the new value is put
in the configuration file gammacoup.yml. At the first usage of the
gammacoup controller the value for this distance as found in the
configuration file, is set in the Redis DB.
Since this value is stored in the Redis DB, it is therefore persistent
across stopping/restarting BLISS session. To be sure to use good values
for this distance (when changed!) in addition to edit gammacoup.yml file,
one should set it directly in the BLISS shell as follows
(example value is from July 2018):
gamma.controller.samptor3 = 700.0

Having __repr__() function allows to see the distance
samptor3 by typing inside BLISS shell:
gamma.controller

"""

from bliss import current_session
from bliss.common.logtools import log_debug, log_info
from bliss.config import settings
from bliss.controllers.motor import CalcController
from bliss.config.settings import SimpleSetting
from bliss.common.hook import MotionHook

from numpy import *

class GammaCouplingOnHook(MotionHook):

    def coupling_on(self):
        self._coupling_mode.set('ON')

    def coupling_off(self):
        self._coupling_mode.set('OFF')

    def is_coupling_on(self):
        return self._coupling_mode.get() != 'OFF'

    def __init__(self, name, config):
        super().__init__()
        
    def pre_move(self, motion_list):
        mode = SimpleSetting('%s:gamma_coupling_mode' % current_session.name)
        if len(motion_list) == 3:
            return
        for axis in motion_list:
            if axis.axis.name in ['gamma', 'z3', 'r3']:
                if  mode == 'ON':
                    raise RuntimeError(f"gamma-coupling is ON, you cannot move {axis.axis.name} alone")

class gammacoup(CalcController):

    def __init__(self, *args, **kwargs):
        #CalcController.__init__(self, *args, **kwargs)
        super(gammacoup, self).__init__(*args, **kwargs)

        # get the simple setting from session to switch on/off couple and
        # single move protection
        self._coupling_mode = SimpleSetting('%s:gamma_coupling_mode' % current_session.name)
        
        hash_name =  'id10.gammacoup'
        ctrl_name = self.config.get('name', default=None)
        if ctrl_name:
            hash_name += '.%s' % ctrl_name

        log_info(self, "In gammacoup constructor")
        log_debug(self, "ctrl_name = %s" % ctrl_name)
        log_debug(self, "hash_name = %s" % hash_name)

        self._samptor3 = settings.SimpleSetting(hash_name + '.samptor3')
        if self._samptor3.get() == None:
            log_debug(self, "samptor3 does not exist in settings")
            log_debug(self, "Try to get it from config")
            samptor3 = self.config.get('samptor3')
            if samptor3 == None:
                log_debug(self, "samptor3 does not exist in config")
                return
            else:
                # Put value found in config to settings
                self._samptor3.set(samptor3)

        log_debug(self, "samptor3 = %r" % self._samptor3)


    @property
    def samptor3(self):
        return self._samptor3.get()

    @samptor3.setter
    def samptor3(self, value):
        self._samptor3.set(value)

 
    @property
    def r3(self):
        return self._tagged['r3'][0]

    @property
    def z3(self):
        return self._tagged['z3'][0]



    def calc_from_real(self, positions_dict):
        '''
        Returns calculated/virtual motor "gamma" position from real ones
        ("r3" and "z3").
	Remark: Normally will NEVER derive calculated/virtual motor, 
		but will rather move it and as a consequence the 2 
	 	real motors ("r3" and "z3") will be moved.

        Units:
	    Calculated/virtual rotation motor "gamma" = in degrees.
        '''

        log_info(self, "In gammacoup calc_from_real()")

        if not self.is_coupling_on():
            return dict(delcoup=nan)

	# gamma position value = r3 position value
        gamma_position = positions_dict["r3"]
        
        return dict(gamma=gamma_position)


    def calc_to_real(self, positions_dict):
        '''
        Returns real motors positions of motors "r3" and "z3" (as a 
        dictionary) given virtual one ("gamma") and sample to r3 distance.
        Units:
	    gamma_position = in degrees
	    r3_position    = in degrees
            samptor3       = in mm
	    z3_position    = in mm 

        '''
        if not self.is_coupling_on():
            raise RuntimeError(
                "Gamma Coupling is OFF, please run gamma_coupling_on() to get it back to ON")
        
        log_info(self, "In gammacoup calc_to_real()")

        gamma_position = positions_dict["gamma"]
        r3_position = gamma_position	
        z3_position = self._samptor3.get() * tan(deg2rad(gamma_position))
        return dict(r3=r3_position,z3=z3_position)


    def  __info__(self):
        ctrl_name = self.config.get('name', default=None)
        name = 'GAMMACOUP' + ('' if ctrl_name is None else ' ' + ctrl_name)
        try:
            return "{name}:\n" \
               "  .samptor3 = {0.samptor3} mm\n".format(self,name=name)
        except AttributeError: # in __init__ not yet fully instantiated
            return super().__info__()
