# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) 2020 Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.


"""
Class to move smultaneously real motors: 'delta' (rotation),
'ypipe'(translation), 'ydet'(translation) in a synchronised 
way, so that A DETECTOR ALWAYS POINTS TO A SAMPLE. 

A move is achieved by using the calculated motor 'delcoup',
which has the same (angle) value as real motor 'delta'.

When the value of all 3 real motors: 'delta', 'ypipe', 'ydet' is 0,
the line from sample to detector is along the X-ray BEAM.

When calculated motor 'delcoup' makes a rotation, the real motor 
'delta' makes a rotation for the same amount, while the motors 
'ypipe' and 'ydet' make a corresponding translation, so that 
A DETECTOR ALWAYS POINT TO A SAMPLE and that the pipe (if mounted
between sample and detector) is ALWAYS on the straigt line
between sample and detector (see sketch here below).


Detector
    _____
       |  _____
       |       _____
       |         |  _____
     ydet        |       _____ 
       |       ypipe      /    _____
       |         |      delta       _____ 
    ___|_________|________\______________   Sample <-------- X-RAY BEAM

                 |<--- pipe supp. to sample -->|
       |<---- detector support to sample ----->|


Real motor roles:
* *delta*: rotation of sample support table
* *ypipe*: translation of 'pipe' support along y (horizontal) axis.
* *ydet*:  translation of detector support along y (horizontal) axis.

Calculated motor roles:
* *delcoup*: angle equal to delta [degrees]

Configuration parameters:
* *deloffset*:  delta offset  [deg]
* *samptopipe*: sample-to-pipe support distance [mm]
* *samptodet*:  sample-to-detector support distance [mm]


Example configuration file (deltacoup.yml) from id10:
-----------------------------------------------------

motors:
  - controller:
    class: deltacoup
    package: id10.controllers.motors.deltacoup
    name: deltacoup
    description: EH2 pipe and detector simultaneous lateral 
                 translation to follow delta rotation 
    samptopipe: 4110.   # [mm] measured 03.05.2019
    samptodet: 5535.	# [mm] measured 03.05.2019
    deloffset: 0.       # [degrees]

    axes:
    - name: $delta
      tags: real delta
    - name: $ypipe
      tags: real ypipe
    - name: $ydet
      tags: real ydet
    - name: delcoup
      description: angle equal to delta
      tags:delcoup 
      low_limit: -0.1  # [degrees] the same as of delta
      high_limit: 6.0  # [degrees] the same as of delta
      user_tag:
      - EH2.DELCOUP
      motion_hooks:
        - $deltahook

hooks:
  - name: deltahook
    class: ChangeVelocitiesHook
    package: id10.controllers.motors.deltacoup
    plugin: bliss

-----------------------------------------------------


In order that pipe does not break, the speeds of 2 faster motors
are adapted to the speed of the slowest motor (currently [= in 
June 2018] motor 'ydet' has the slowest configured speed) so that 
all 3 motors move 'synchronously' i.e. with the same angular speed.

Since the BLISS hook fatures is used in this controller the hook
function _set_velocities_before_move() is used for adjustment of 
speeds of the 3 real motors before movement and the hook function
_reset_velocities() is used after the movement to set back the 
speeds of the 3 real motors to the configured values. If one wants
nevertheless to 'manually' reset the 3 real motor speeds to the 
configured values after the movements, one can type:
delcoup.controller._reset_velocities() 
in the BLISS shell (= inside BLISS session).

The BLISS session which wants to use this controller should normally
load the script 'delta_coupling_onoff.py' in its setup file and should
not put motors: 'delta', 'ypipe', 'ydet' and 'delcoup' in the list of 
config-objects in its yml file, but rather rely on the dynamic 
inclusion/exclusion of different motor objects by invoking in the 
BLISS shell the functions:
delta_coupling_on() ... only the calculated motor 'delcoup' is visible in 
                        the BLISS shell and can be moved. As a consequence
                        of its move, the 3 real motors: 'delta', 'ypipe', 
                        'ydet' are moved in a coupled/synchronous way.
delta_coupling_off() ... only 3 physical motors: 'delta', 'ypipe', 'ydet' 
                         are visible in the BLISS shell and can be moved 
                         independently. There is no more coupling among them.
The last used situation (coupling ON or OFF) is stored in the settings
(= in the Redis DB) and can be checked by typing in the BLISS shell:
is_delta_coupling_on() ... invoking this funcion tells if the coupling of 
                           the 3 real motors to the calculated motor 
                           'delcoup' is ON (answer = True) or 
                                       OFF (answer = False).
The script 'delta_coupling_onoff.py' is in the 'standard' place:
~blissadm:local/beamline_configuration/sessions/scripts

Example session (testdc) configuration file (testdc.yml) from id10:
-------------------------------------------------------------------
class: Session
name: testdc
setup-file: ./testdc.py
config-objects: []


Example session (testdc) setup file (testdc.py) from id10:
----------------------------------------------------------
print("Wellcome to testdc session setup file")
load_script('delta_coupling_onoff')

Even when coupling is ON and therefore 3 physical motors:
'delta', 'ypipe', 'ydet' are not directly accesible, one can 
reach them at controller level. For ex. if want to see the 
delta position, we type on the BLISS shell:
delcoup.controller.delta.position 

Since sample to pipe and/or sample to detector distance can change,
after each measurement of these distances the new values are put 
in the configuration file deltacoup.yml. At the first usage of the
deltacoup controller the values for these distances as found in the
configuration file, are set in the Redis DB.
Since these value are stored in the Redis DB, they are therefore persistent
across stopping/restarting BLISS session. To be sure to use good values
for these distances (when changed!) in addition to edit deltacoup.yml file, 
one should set them directly in the BLISS shell as follows 
(example values are from 3rd May 2019):
delcoup.controller.samptopipe = 4110.0
delcoup.controller.samptodet = 5330.0
delcoup.controller.deloffset = 0.0

Having __repr__() function allows to see both distances 
(samptopipe, samptodet) and delta offset angle (deloffset)
in one go by typing inside BLISS shell:
delcoup.controller

"""

from bliss import current_session
from bliss.config import settings
from bliss.common.logtools import log_debug, log_info
from bliss.controllers.motor import CalcController
from bliss.config.settings import SimpleSetting
from bliss.common.hook import MotionHook

from numpy import *


class ChangeVelocitiesHook(MotionHook):
    def __init__(self, name, config):
        super().__init__()

    def pre_move(self, motion_list):
        motion = motion_list[0]
        controller = motion.axis.controller
        #motion.axis.controller._set_velocities_before_move(motion.target_pos)
        controller._set_velocities_before_move(motion.target_pos)
 
    def post_move(self, motion_list):
        motion = motion_list[0]
        controller = motion.axis.controller
        #motion.axis.controller._reset_velocities()
        controller._reset_velocities()

class DeltaCouplingOnHook(MotionHook):
    def __init__(self, name, config):
        super().__init__()
        
    def pre_move(self, motion_list):
        mode = SimpleSetting('%s:delta_coupling_mode' % current_session.name)
        if len(motion_list) == 2:
            return
        for axis in motion_list:
            if axis.axis.name in ['delta_eh2', 'ydet', 'ypipe']:
                if  mode.get() == 'ON':
                    raise RuntimeError(f"delta-coupling is ON, you cannot move {axis.axis.name} alone")

 

class deltacoup(CalcController):

    def coupling_on(self):
        self._set_velocities()
        self._coupling_mode.set('ON')

    def coupling_off(self):
        self._reset_velocities()
        self._coupling_mode.set('OFF')

    def is_coupling_on(self):
        return self._coupling_mode.get() != 'OFF'

    def __init__(self, *args, **kwargs):
        super(deltacoup, self).__init__(*args, **kwargs)

        # get the simple setting from session to switch on/off couple and
        # single move protection
        self._coupling_mode = SimpleSetting('%s:delta_coupling_mode' % current_session.name)
        
        hash_name =  'id10.deltacoup'
        ctrl_name = self.config.get('name', default=None)
        if ctrl_name:
            hash_name += '.%s' % ctrl_name

        log_info(self, "In deltacoup constructor")
        log_debug(self, "ctrl_name = %s" % ctrl_name)
        log_debug(self, "hash_name = %s" % hash_name)

        self._samptopipe = settings.SimpleSetting(hash_name + '.samptopipe')
        if self._samptopipe.get() == None:
            log_debug(self, "samptopipe does not exist in settings")
            log_debug(self, "Try to get it from config")
            samptopipe = self.config.get('samptopipe')
            if samptopipe == None:
                log_debug(self, "samptopipe does not exist in config")
                return
            else:
                # Put value found in config to settings
                self._samptopipe.set(samptopipe)
        
        self._samptodet = settings.SimpleSetting(hash_name + '.samptodet')
        if self._samptodet.get() == None:
            log_debug(self, "samptodet does not exist in settings")
            log_debug(self, "Try to get it from config")
            samptodet = self.config.get('samptodet')
            if samptodet == None:
                log_debug(self, "samptodet does not exist in config")
                return
            else:
                # Put value found in config to settings
                self._samptodet.set(samptodet)

        self._deloffset = settings.SimpleSetting(hash_name + '.deloffset')
        if self._deloffset.get() == None:
            log_debug(self, "deloffset does not exist in settings")
            log_debug(self, "Try to get it from config")
            deloffset = self.config.get('deloffset')
            if deloffset == None:
                log_debug(self, "deloffset does not exist in config")
                return
            else:
                # Put value found in config to settings
                self._deloffset.set(deloffset)

        log_debug(self, "Values of 2 distances and 1 angle offset:")
        log_debug(self, "-----------------------------------------")
        log_debug(self, "samptopipe = %r" % self._samptopipe)
        log_debug(self, "samptodet  = %r" % self._samptodet)
        log_debug(self, "deloffset  = %r" % self._deloffset)


    @property
    def samptopipe(self):
        return self._samptopipe.get()

    @samptopipe.setter
    def samptopipe(self, value):
        self._samptopipe.set(value)


    @property
    def samptodet(self):
        return self._samptodet.get()

    @samptodet.setter
    def samptodet(self, value):
        self._samptodet.set(value)


    @property
    def deloffset(self):
        return self._deloffset.get()

    @deloffset.setter
    def deloffset(self, value):
        self._deloffset.set(value)


    @property
    def delta(self):
        return self._tagged['delta'][0]

    @property
    def ypipe(self):
        return self._tagged['ypipe'][0]

    @property
    def ydet(self):
        return self._tagged['ydet'][0]



    def calc_from_real(self, positions_dict):
        '''
        Returns calculated/virtual motor "delcoup" position from real one
        ("delta").
        Remark: Normally will NEVER derive calculated/virtual motor,
                but will rather move it and as a consequence the 3
                real motors ("delta", "ypipe" and "ydet") will be moved.
        Units:
            Calculated/virtual rotation motor "delcoup" = in degrees.
        '''

        #log_info(self, "In deltacoup calc_from_real()")

        #if not self.is_coupling_on():
        #    return dict(delcoup=nan)
         
        # delcoup position value = delta position value
        delcoup_position = positions_dict["delta"]

        return dict(delcoup=delcoup_position)



    def calc_to_real(self, positions_dict):
        '''
        Returns real motors positions of motors "delta", "ypipe" and "ydet"
        (as a dictionary) given the position of the virtual one ("delcoup")
        and sample to ypipe and sample to detector distances.
        Units:
            delta_position = in degrees
            delcoup_position  = in degrees
            ypipe_position = in mm
            ydet_position = in mm
        '''
        if not self.is_coupling_on():
            raise RuntimeError(
                "Delta Coupling is OFF, please run delta_coupling_on() to get it back to ON")


        delta = self.delta
        ypipe = self.ypipe
        ydet  = self.ydet

        # Destination positions
        delcoup_position = positions_dict["delcoup"]
        delta_position = delcoup_position
        ypipe_position = self.samptopipe * tan(deg2rad(delcoup_position-self.deloffset))
        ydet_position = self.samptodet * tan(deg2rad(delcoup_position-self.deloffset))

        return dict(delta=delta_position,ypipe=ypipe_position,ydet=ydet_position)



    def _set_velocities_before_move(self, target_pos):
        '''
        This function calculates and set the velocities
        for each real motor (delta, ypipe, ydet) in order
        to have their synchronized movement (i.e. the same
        angular velocity). The velocities are adapted to the
        slowest motor.
        '''

        log_info(self, "In deltacoup _set_velocities_before_move()")

        delta = self.delta
        ypipe = self.ypipe
        ydet  = self.ydet

        delta_curr_pos = delta.position
        ypipe_curr_pos = ypipe.position
        ydet_curr_pos = ydet.position

        delta_curr_vel = delta.velocity
        ypipe_curr_vel = ypipe.velocity
        ydet_curr_vel = ydet.velocity

        log_debug(self, "Current positions: delta=%f,ypipe=%f,ydet=%f" % \
               (delta_curr_pos,ypipe_curr_pos,ydet_curr_pos))
        log_debug(self, "Current velocities: delta=%f,ypipe=%f,ydet=%f" %\
               (delta_curr_vel,ypipe_curr_vel,ydet_curr_vel))
	
        # Destination positions
        delcoup_position = target_pos
        delta_position = delcoup_position
        ypipe_position = self.samptopipe * tan(deg2rad(delcoup_position-self.deloffset))
        ydet_position = self.samptodet * tan(deg2rad(delcoup_position-self.deloffset))

        # Estimate time for movement of each real motor
        delta_time = abs(delta_position - delta_curr_pos)/delta_curr_vel
        ypipe_time = abs(ypipe_position - ypipe_curr_pos)/ypipe_curr_vel
        ydet_time = abs(ydet_position - ydet_curr_pos)/ydet_curr_vel
        log_debug(self, "Estimated time for move: delta=%f,ypipe=%f,ydet=%f" % (delta_time,ypipe_time,ydet_time))

        # Find the maximum of 3 times
        max_time = max(delta_time, ypipe_time, ydet_time)
        log_debug(self, "Maximal time: %f" % max_time)
        if max_time == ydet_time:
            # Adapt the speed of delta and ypipe
            delta_new_vel = delta_curr_vel * (delta_time/ydet_time)
            ypipe_new_vel = ypipe_curr_vel * (ypipe_time/ydet_time)
            log_debug(self, "New velocities: delta=%f,ypipe=%f" % \
                   (delta_new_vel,ypipe_new_vel))
            delta.velocity = delta_new_vel
            ypipe.velocity = ypipe_new_vel
        elif max_time == ypipe_time:
            # Adapt the speed of delta and ydet
            delta_new_vel = delta_curr_vel * (delta_time/ypipe_time)
            ydet_new_vel = ydet_curr_vel * (ydet_time/ypipe_time)
            log_debug(self, "New velocities: delta=%f,ydet=%f" % \
                   (delta_new_vel,ydet_new_vel))
            delta.velocity = delta_new_vel
            ydet.velocity = ydet_new_vel
        else:
            # Adapt the speed of ypipe and ydet
            ypipe_new_vel = ypipe_curr_vel * (ypipe_time/delta_time)
            ydet_new_vel = ydet_curr_vel * (ydet_time/delta_time)
            log_debug(self, "New velocities: ypipe=%f,ydet=%f" % \
                   (ypipe_new_vel,ydet_new_vel))
            ypipe.velocity = ypipe_new_vel
            ydet.velocity = ydet_new_vel

    def _set_velocities(self):
        '''
        This function calculates and set the velocities
        for each real motor (delta, ypipe, ydet) in order
        to have their synchronized movement (i.e. the same
        angular velocity). The velocities are adapted to the
        slowest motor.
        '''

        log_info(self, "In deltacoup _set_velocities()")

        delta = self.delta
        ypipe = self.ypipe
        ydet  = self.ydet

        delta_curr_pos = delta.position
        ypipe_curr_pos = ypipe.position
        ydet_curr_pos = ydet.position

        delta_curr_vel = delta.velocity
        ypipe_curr_vel = ypipe.velocity
        ydet_curr_vel = ydet.velocity

        log_debug(self, "Current positions: delta=%f,ypipe=%f,ydet=%f" % \
               (delta_curr_pos,ypipe_curr_pos,ydet_curr_pos))
        log_debug(self, "Current velocities: delta=%f,ypipe=%f,ydet=%f" %\
               (delta_curr_vel,ypipe_curr_vel,ydet_curr_vel))
	
        # Just calculated travels for 1.0 degree of rotation
        delcoup_rot = 1.0
        ypipe_position = self.samptopipe * tan(deg2rad(delcoup_rot))
        ydet_position = self.samptodet * tan(deg2rad(delcoup_rot))

        # Estimate time for movement of each real motor
        delta_time = abs(delcoup_rot)/delta_curr_vel
        ypipe_time = abs(ypipe_position - ypipe_curr_pos)/ypipe_curr_vel
        ydet_time = abs(ydet_position - ydet_curr_pos)/ydet_curr_vel
        log_debug(self, "Estimated time for move: delta=%f,ypipe=%f,ydet=%f" % (delta_time,ypipe_time,ydet_time))

        # Find the maximum of 3 times
        max_time = max(delta_time, ypipe_time, ydet_time)
        log_debug(self, "Maximal time: %f" % max_time)
        if max_time == ydet_time:
            # Adapt the speed of delta and ypipe
            delta_new_vel = delta_curr_vel * (delta_time/ydet_time)
            ypipe_new_vel = ypipe_curr_vel * (ypipe_time/ydet_time)
            log_debug(self, "New velocities: delta=%f,ypipe=%f" % \
                   (delta_new_vel,ypipe_new_vel))
            delta.velocity = delta_new_vel
            ypipe.velocity = ypipe_new_vel
        elif max_time == ypipe_time:
            # Adapt the speed of delta and ydet
            delta_new_vel = delta_curr_vel * (delta_time/ypipe_time)
            ydet_new_vel = ydet_curr_vel * (ydet_time/ypipe_time)
            log_debug(self, "New velocities: delta=%f,ydet=%f" % \
                   (delta_new_vel,ydet_new_vel))
            delta.velocity = delta_new_vel
            ydet.velocity = ydet_new_vel
        else:
            # Adapt the speed of ypipe and ydet
            ypipe_new_vel = ypipe_curr_vel * (ypipe_time/delta_time)
            ydet_new_vel = ydet_curr_vel * (ydet_time/delta_time)
            log_debug(self, "New velocities: ypipe=%f,ydet=%f" % \
                   (ypipe_new_vel,ydet_new_vel))
            ypipe.velocity = ypipe_new_vel
            ydet.velocity = ydet_new_vel



    def _reset_velocities(self):
        '''
        This function serves to reset the velocities
        of the 3 physical motors to the values as are
        in the config (yml) file.
        Usage inside the BLISS session:
        delcoup.controller._reset_velocities()
        '''
    
        log_info(self, "In deltacoup _reset_velocities()")
    
        delta = self.delta
        ypipe = self.ypipe
        ydet  = self.ydet

        delta_config_vel = delta.config.get('velocity')
        ypipe_config_vel = ypipe.config.get('velocity')
        ydet_config_vel = ydet.config.get('velocity')

        delta.velocity = float(delta_config_vel)
        ypipe.velocity = float(ypipe_config_vel)
        ydet.velocity = float(ydet_config_vel)



    def __info__(self):
        '''
        This function serves to show the angle and 2 distances:
        deloffset, samptopipe and samptodet
        Usage inside the BLISS session:
        delcoup.controller
        '''

        ctrl_name = self.config.get('name', default=None)
        name = 'DELTACOUP' + ('' if ctrl_name is None else ' ' + ctrl_name)
        try:
            return "{name}:\n" \
                "  .deloffset = {0.deloffset} deg\n" \
                "  .samptopipe = {0.samptopipe} mm\n" \
                "  .samptodet = {0.samptodet} mm\n".format(self,name=name)
        except AttributeError:  # in __init__ not yet fully instantiated
            return super().__info__()

