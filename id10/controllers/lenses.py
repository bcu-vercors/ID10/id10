# -*- coding: utf-8 -*-
"""Class for handling ID10 objects with multiple positions."""


import numpy as np

from .multiplepositions import MultiplePositions

class Lenses(MultiplePositions):

    def __init__(self, config, name=None, positions=None,
                 distance_from_sample = None,
                 css_distance=None, safety_shutters=None):

        """

        Parameters
        ----------
        config : bliss.config.static.Config or None
            Proxy to the bliss.config.static.Config associated to the
            session.
            If 'config' is not None (default), all other parameters are
            taken from the YAML file.`
            If 'config' is None, 'atts' and 'labels' cannot be None.
        name : str or None
            Object name.
        positions : list or None
            List of dictionary defining the different possible predefined
            positions of the obj.
        css_distance : float or None.
            Distance of the object from the center of the straight
            session (CSS).
        safety_shutters : list or None.
            List of safety shutter to close before moving (they will be
            reopened once done if initially open).

        """

        super().__init__(config, name=name, positions=positions,
                 css_distance=css_distance, safety_shutters=safety_shutters)
        
        self.distance_from_sample = distance_from_sample if config is None else config.get("distance_from_sample")
        self._define_lenses()

    def _get_mono_E():
        import id10utils
        return id10utils.xray.mono_energy()

    def _define_lenses(self):
        from sr.crl import LensBlock
        self._lenses = []
        for slot in self._positions:
            nl = slot.get("nlenses",0)
            r  = slot.get("radius",None)
            mat = slot.get("material","Be")
            density = slot.get("density",None)
            if density is not None: density = density/1e3
            if nl == 0 or r is None:
                lenses = None
            else:
                lenses = LensBlock(
                     n = nl,
                     radius = r,
                     material = mat,
                     density = density
                )
            self._lenses.append(lenses)

    def get_transmission(self,E="auto",slot="auto"):
        import id10utils
        if slot == "auto": slot = self._labels.index(self.position)
        if E == "auto": E = id10utils.xray.mono_energy()
        print(slot)
        lenses = self._lenses[slot]
        return lenses.transmission_central_ray(E)


    def get_lenses(self,slot="auto"):
        if slot == "auto": slot = self._labels.index(self.position)
        lenses = self._lenses[slot]
        return lenses

    @property
    def lenses(self):
        return self.get_lenses()
