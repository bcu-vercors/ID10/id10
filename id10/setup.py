# -*- coding: utf-8 -*-
#
# This file is part of the ESRF-ID10 project
#
# Copyright (c) 2018 ID31, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

import os
import errno
import logging
import functools

from six import print_

from bliss import setup_globals
from bliss.config.static import get_config
from bliss.controllers.lima import Lima
from bliss.controllers.lima.roi import Roi

__all__ = ['BEAMLINE', 'log', 'FastShutter', 'setup', 'new_sample', 'new_experiment']

# for now, do not bother users with deprecation warnings
import warnings
warnings.resetwarnings()
warnings.simplefilter('ignore')


def makedirs(path):
    if os.path.exists(path):
        if not os.path.isdir(path):
            raise OSError(errno.ENOTDIR,
                          '%r exists and is not a directory' % path)
    else:
        os.makedirs(path)


def build_scan_saving_prefix(scan_saving):
    beamline = scan_saving.beamline.lower()
    experiment = scan_saving.experiment.lower()
    sample = scan_saving.sample
    prefix = os.path.join(beamline, 'inhouse', 'exp')
    if experiment.startswith('blc'):
        prefix = os.path.join(prefix, 'BLC', experiment)
    elif experiment.startswith('ihr'):
        prefix = os.path.join(prefix, 'IHR', experiment)
    else:
        prefix = os.path.join('visitor', experiment, beamline)
    return os.path.join(prefix, sample)


def setup(**kwargs):
    pass
    #setup_paths(**kwargs)


def setup_paths(subdirs=(), **kwargs):
    def add(param, default=None):
        if param in kwargs or not hasattr(scan_saving, param):
            scan_saving.add(param, kwargs.get(param, default))

    scan_saving = setup_globals.SCAN_SAVING
    add('base_path', '/data')
    add('beamline', BEAMLINE)
    add('experiment', 'ihr0000')
    add('sample', 'Default')
    scan_saving.add('prefix', build_scan_saving_prefix)
    scan_saving.template = '{prefix}'

    # make sure path exists
    base_path = scan_saving.get_path()
    for path in set([''] + list(subdirs)):
        try:
            makedirs(os.path.join(base_path, path))
        except OSError as ose:
            log.error('Scans may not be saved correctly: %s', ose)
        else:
            log.info('Scans will be saved in %r', path)


def new_experiment(experiment, sample=None):
    kwargs = dict(experiment=experiment)
    if sample:
        kwargs['sample'] = sample
    setup_paths(**kwargs)


def new_sample(sample, experiment=None):
    """
    Activate a new sample with the given sample name.
    If experiment is None (default) the experiment name is not changed.
    """
    kwargs = dict(sample=sample)
    if experiment:
        kwargs['experiment'] = experiment
    setup_paths(**kwargs)


