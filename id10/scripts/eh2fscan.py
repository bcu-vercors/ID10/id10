import numpy
import sys
import datetime
import time
import click

from bliss.common import scans
from bliss import setup_globals
from bliss.scanning.scan import ScanPreset
from bliss.scanning.chain import ChainPreset
from bliss import current_session
from bliss import setup_globals

from bliss import current_session

from bliss.shell.cli.user_dialog import UserMsg, UserYesNo
from bliss.shell.cli.pt_widgets import display

from fscan.fscantools import FScanMode
from fscan.mtimescan import MTimeScanMaster


def fasttimescan(nbpoints, exp_time, latency_time=0, shutter_mode='sequence', verbose=True, confirm=True, save=True):
    """
    Start hardware triggered time scan on detectors with MUSST card as the master.
    To select the detectors, enable/disable them using the ACTIVE_MG measurement group.
    Args:
        nbpoints: number of acquistion points
        exp_time: exposure time in second
        latency_time:  time to wait between 2 points [default: 0]
        shutter_mode: shutter can be open/close for every points ('frame') or for the whole scan ('sequence')
        save: choose to save or not the data [default: True]
    Returns:
        Scan: the scan handler
    """

    ftimescan = setup_globals.ftimescan
    fscan_mux = setup_globals.fscan_mux

    # Add a latency if requested, no longer change the period, ftimescan use the camera calibration (mpx, eiger, pilatus ...)
    #if latency_time > 0:
    ftimescan.pars.latency_time = latency_time

    ftimescan.pars.acq_time = exp_time
    ftimescan.pars.npoints = nbpoints
    ftimescan.pars.sampling_time = 0.5
    ftimescan.pars.save_flag = save

    if verbose:
        ftimescan.show()

    ftimescan.prepare()
    if verbose:
        ftimescan.chain_show()

    if not confirm or (confirm and click.confirm('Do you want to continue?', default=True)):

        # set shutter mode to mux
        fscan_mux.set_shutter_mode(shutter_mode)
        ftimescan.start()
        return ftimescan.scan

    return None


class CountMuxPreset(ChainPreset):
    def __init__(self, mux):
        self.mux = mux
        self._acq_mode = 'COUNT'
        ScanPreset.__init__(self)

    @property
    def acq_mode(self):
        return self._acq_mode
    
    @acq_mode.setter
    def acq_mode(self, mode):
        assert isinstance(mode, str)
        assert (mode.upper() == 'COUNT' or mode.upper() == 'ACCUMULATION')
        self._acq_mode = mode
        
    def prepare(self, chain):
        self.mux.switch('SHUTTER_MODE', 'ALL')            
        self.mux.switch('ACQ_TRIG_EH2',self._acq_mode)


class FScanMuxPreset(ScanPreset):
    def __init__(self, mux):
        self.mux = mux
        ScanPreset.__init__(self)
        self.shutter_mode = 'sequence'
        self.is_mtimescan = False
        
    def set_shutter_mode(self, mode):
        self.shutter_mode = mode

    def set_fscan_master(self, master):
        if isinstance(master, MTimeScanMaster):
            self.is_mtimescan = True
        else:
            self.is_mtimescan = False
        
    def prepare(self, chain):
        pass

    def start(self, chain):
        # opiom prg must be changed, this mode send ATRIG to P201
        # which is a big mistake since fscan program the p201 in ExtGate
        # That mode was only used with andor cameras and where shutter is
        # triggered (gate) by the BTRIG musst output, for slow scan (frame mode) of course !!!
        # L.Claustre 29/06/2022
        #self.mux.switch('ACQ_TRIG_EH2','FSCAN_SHUTTER_DELAY')
        if self.is_mtimescan:
            muxname = "MSCAN"
        else:
            muxname = "FSCAN"
        self.mux.switch('ACQ_TRIG_EH2', muxname)
        
        if self.shutter_mode == 'sequence':
            self.mux.switch('SHUTTER_MODE', 'MANUAL')
            self.mux.switch('SHUTTER_MANUAL', 'OPEN')
            print('Shutter open')
        else: 
            self.mux.switch('SHUTTER_MODE', 'FSCAN')              
    
    def stop(self, acq_chain):
        if self.shutter_mode == 'sequence':
            print('Shutter closed')
            self.mux.switch('SHUTTER_MANUAL', 'CLOSE')
            
        self.mux.switch('SHUTTER_MODE', 'ALL')            



def cdifastscan(start, stop, nb_points, expo_time, latency_time=0, save=True, verbose=False, confirm=True):
    """
    Start continuous scan of the CDI ths rotation and trig the detectors.
    To select the detectors, add/remmove or enable/disable in the ACTIVE_MG measurement group.
    Args:
        start: the motor start position
        stop:  the motor stop position
        nb_points: number of acquistion points
        exp_time: exposure time in second
        latency_time:  time to wait between 2 points [default: 0]
        save: choose to save or not the data [default: True]
    Returns:
        Scan: the scan handler
    """

    # adjust the step size according to start/stop/nbpoints
    step_size = abs(stop-start)/(nb_points-1)


    fscan = setup_globals.fscan
    
    #
    # set parameters for fscan
    #
    fscan.pars.motor        = setup_globals.ths
    fscan.pars.start_pos    = start
    fscan.pars.step_size    = step_size
    fscan.pars.npoints      = nb_points
    fscan.pars.acq_time     = expo_time
    if latency_time > 0:
        fscan.pars.latency_time = latency_time

    # always generate trig in time (default is TIME)
    fscan.pars.scan_mode    = FScanMode.TIME

    # always generate gate according to step_time and not to step_size (default is TIME)
    fscan.pars.gate_mode    = FScanMode.TIME
    
    # synchronize musst counter with real motor position (default is True)
    fscan.pars.sync_encoder = True
    
    # do not deal with an acceleration margin, but to be tested with Micos
    fscan.pars.acc_margin   = 0
    
    fscan.pars.save_flag    = save
    
    if verbose:
        fscan.show()
        
    fscan.prepare()
    if verbose:
        fscan.chain_show()

    if not confirm or (confirm and click.confirm('Do you want to continue?', default=True)):        
        fscan.start()
        return fscan.scan

    return None    
