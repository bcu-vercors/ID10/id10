import numpy
import sys
import datetime
import time
import click

from bliss.common import scans
from bliss import setup_globals
from bliss.scanning.scan import ScanPreset
from bliss.scanning.chain import ChainPreset
from bliss import current_session

from bliss.shell.cli.user_dialog import UserMsg, UserYesNo
from bliss.shell.cli.pt_widgets import display


from fscan.fscantools import FScanMode


def fastscan(motor, start, stop, nb_points, expo_time, latency_time=0, save=True, verbose=False, confirm=True):
    """
    Start hardware triggered scan on detectors with MUSST card as the master.
    To select the detectors, enable/disable them using the ACTIVE_MG measurement group.
    Args:
        motor: the motor to move
        start: the motor start position
        stop:  the motor stop position
        nb_points: number of acquistion points
        exp_time: exposure time in second
        latency_time:  time to wait between 2 points [default: 0]
        save: choose to save or not the data [default: True]
    Returns:
        Scan: the scan handler
    """

    # todo calculate the minimum lima latency to set the step_time bigger than the acq_time
    camera_latency = dict()

    # adjust the step size according to start/stop/nbpoints
    step_size = abs(stop-start)/(nb_points-1)


    fscan = setup_globals.fscan
    
    #
    # set parameters for fscan
    #
    fscan.pars.motor        = motor
    fscan.pars.start_pos    = start
    fscan.pars.step_size    = step_size
    fscan.pars.npoints      = nb_points
    fscan.pars.acq_time     = expo_time
    if latency_time > 0:
        fscan.pars.latency_time = latency_time

    # always generate trig in time (default is TIME)
    fscan.pars.scan_mode    = FScanMode.TIME

    # always generate gate according to step_time and not to step_size (default is TIME)
    fscan.pars.gate_mode    = FScanMode.TIME
    
    # synchronize musst counter with real motor position (default is True)
    fscan.pars.sync_encoder = True
    
    # do not deal with an acceleration margin, but to be tested with Micos
    fscan.pars.acc_margin   = 0
    
    fscan.pars.save_flag    = save

    if verbose:
        fscan.show()
        
    fscan.prepare()
    if verbose:
        fscan.chain_show()

    if not confirm or (confirm and click.confirm('Do you want to continue?', default=True)):        
        fscan.start()
        return fscan.scan

    return None

class CountMuxPreset(ChainPreset):
    def __init__(self, mux):
        self.mux = mux
        self._acq_mode = 'COUNT'
        ChainPreset.__init__(self)
                
    @property
    def acq_mode(self):
        return self._acq_mode
    
    @acq_mode.setter
    def acq_mode(self, mode):
        assert isinstance(mode, str)
        assert (mode.upper() == 'COUNT' or mode.upper() == 'ACCUMULATION')
        self._acq_mode = mode
        
    def prepare(self, chain):
        self.mux.switch('ACQ_TRIG_EH1',self._acq_mode)
        

class FScanMuxPreset(ScanPreset):
    def __init__(self, mux):
        self.mux = mux
        ScanPreset.__init__(self)
                
    def prepare(self, scan):
        pass

    def start(self, scan):
        self.mux.switch('ACQ_TRIG_EH1','FSCAN')
        
    def stop(self, scan):
        self.mux.switch('ACQ_TRIG_EH1','COUNT')
