from bliss import setup_globals
from bliss.scanning.acquisition.lima import LimaAcquisitionMaster
from bliss.scanning.acquisition.motor import LinearStepTriggerMaster
from bliss.scanning.acquisition.counter import IntegratingCounterAcquisitionDevice
from bliss.scanning.scan import Scan
from bliss.scanning import scan as scan_module
from bliss.scanning.chain import AcquisitionChain

def scan_threshold(start_pos, end_pos, nb_points, expo_time):
    chain = AcquisitionChain()

    mpxthl = setup_globals.mpxthl
    master = LinearStepTriggerMaster(nb_points,
                                     mpxthl, start_pos, end_pos)
    mpx22 = setup_globals.mpx22
    det_acq = LimaAcquisitionMaster(mpx22,
                                    acq_trigger_mode='INTERNAL_TRIGGER',
                                    acq_expo_time=expo_time,
                                    acq_nb_frames=1, save_flag=False)
    det_acq.add_counter(mpx22.image)

    chain.add(master, det_acq)
    
    roi_counters = mpx22.counter_groups.roi_counters
    if roi_counters:
        det_counters = IntegratingCounterAcquisitionDevice(roi_counters,
                                                           npoints=1,count_time=expo_time,
                                                           prepare_once=False, start_once=False) 
        chain.add(det_acq, det_counters)

    saving_parameters = setup_globals.SCAN_SAVING.get()
    scan = Scan(chain,name='scan_threshold',
                writer=saving_parameters['writer'],
                parent=saving_parameters['parent'],
                data_watch_callback=scan_module.StepScanDataWatch())
    scan.run()
