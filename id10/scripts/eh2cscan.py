import numpy
import sys
import datetime
import time

from bliss.scanning.chain import AcquisitionChain
from bliss.scanning.chain import ChainPreset
from bliss.scanning.scan import Scan, StepScanDataWatch
from bliss.config.settings import ParametersWardrobe as Parameters
from bliss.scanning.acquisition.lima import LimaAcquisitionMaster
from bliss.scanning.acquisition.ct2 import CT2AcquisitionMaster
from bliss.scanning.acquisition.musst import MusstAcquisitionMaster
from bliss.scanning.acquisition.musst import MusstAcquisitionSlave
from bliss.scanning.acquisition.counter import IntegratingCounterAcquisitionSlave
from bliss.controllers.ct2.device import AcqMode
from bliss.common import scans

from bliss import setup_globals

from bliss import current_session

from bliss.shell.cli.user_dialog import UserMsg, UserYesNo
from bliss.shell.cli.pt_widgets import display

zaptime_parameters = Parameters('%s:zaptime' % CURRENT_SESSION.name,
                                default_values =
                                {'shutter_delay':7e-3,
                                'saving_format': 'edfgz'}
                                )



def zaptime(nbpoints, exp_time, latency_time=0, shutter_mode='sequence',  save=True):
    """
    Start hardware triggered time scan on detectors with MUSST card as the master.
    To select the detectors, enable/disable them using the ACTIVE_MG measurement group.
    Args:
        nbpoints: number of acquistion points
        exp_time: exposure time in second
        latency_time:  time to wait between 2 points [default: 0]
        shutter_mode: shutter can be open/close for every points ('frame') or for the whole scan ('sequence')
        save: choose to save or not the data [default: True]
    Returns:
        Scan: the scan handler
    """

    camera_latency = dict()
    measurement = setup_globals.ACTIVE_MG
    enabled_counters_name = measurement.enabled
    # if not 2D detector enabled, set min. latency reasonable for P201
    min_latency = 10e-6
    
    for detname in ['mpx_si_22', 'mpx_cdte_22_eh2','andor1', 'andor2', 'eiger1','eiger1_dummy']:
        if detname in enabled_counters_name:
            enabled_counters_name.remove(detname)
            det = getattr(setup_globals,detname)
            acq_expo_time = exp_time
            if detname.startswith('andor'):
                det_latency = 0.5
            else:
                det_latency = det.proxy.latency_time
            #for eiger always add 5us of latency otherwise it can miss triggers
            if detname.startswith('eiger'):
                det_latency += 5e-6
            if detname.startswith('mpx'):
                det_latency += 500e-6
            print(detname, det_latency)
                    
            camera_latency[det] = det_latency

    if camera_latency.keys():
        min_latency = max(camera_latency.values())
    else:
        dlg = UserYesNo(label="you are running a scan without 2D detector\nDo you want to continue ?")
        ret = display(dlg,title='Confirm scan')
        if not ret: return None
        
    if latency_time != 0 and latency_time < min_latency:
        raise RuntimeError("latency_time must be greater than %f sec. (readout time of the 2D detectors)"% min_latency)
    if latency_time == 0:
        latency_time = min_latency
        
    if shutter_mode == 'sequence':
        print('Shutter in SEQUENCE mode')
        shutter_delay = 0
        delay  = latency_time
    else: # 'frame'
        print('Shutter in FRAME mode')
        shutter_delay = zaptime_parameters.shutter_delay
        delay = latency_time
        
    
    # Ok the final sampling period is :
    global_delay = delay if shutter_mode=='sequence' else delay + shutter_delay
    interval_time = exp_time + global_delay
    total_time = interval_time * (nbpoints-1)
    
    chain = AcquisitionChain()

    # Add a preset to manage shutter and multiplexer (eh2 opiom)
    preset = MultiplexerPreset(shutter_mode)
    chain.add_preset(preset)
    
    # MUSST: create a master
    musst_board = setup_globals.musst_eh2
    musst_clock = musst_board.get_timer_factor()
    # Adapt the clock frequency to the acquisition time
    clock_found = False
    for musst_new_clock in [(50e6,'50MHZ'),(10e6,'10MHZ'),(1e6,'1MHZ'),(100e3,'100KHZ'),(10e3,'10KHZ'),(1e3,'1KHZ')]:
        # 31 bit because seb is reading the musst timer in signed
        max_total_time = (2**31)/musst_new_clock[0]
        if max_total_time > total_time:
            clock_found = True
            break
    if clock_found:
        musst_board.TMRCFG = musst_new_clock[1]
        print ('Musst board set to {0}, for a max. acq. time of {1:.2f} mn'.format(musst_board.TMRCFG[0], max_total_time/60))
    else:
        print ('Sorry, cannot manage a so long acquisition time {0:.2f} mn, \
        please change the zaptime parameters, maximum is {1:.2f} mn'.format(total_time/60,max_total_time/60))
        return
        
    
    # round (ceil) timing here to the upper tick of the musst clock
    musst_delay = int(numpy.ceil(delay * musst_clock))
    delay = musst_delay / musst_clock
    musst_shutter_delay = int(numpy.ceil(shutter_delay * musst_clock))
    shutter_delay = musst_shutter_delay / musst_clock
    exposure_time =  int(exp_time * musst_clock)
    
    vars = {'SHUTTER_DELAY':musst_shutter_delay,
            'DELAY': musst_delay,
            'EXPO_TIME': exposure_time,
            'NB_POINTS': int(nbpoints)
    }
    musst_master = MusstAcquisitionMaster(musst_board,
                                          program = 'zaptime.mprg',
                                          program_start_name = 'NOSHUTTER' if shutter_mode=='sequence' else 'SHUTTER',
                                          program_abort_name = 'CLEAN',
                                          vars=vars)
    
    # MUSST: create a musst device to read the store list and time-stamps
    # of trigger
    # set storelist to get timer 
    store_list=['time']
    musst_device  = MusstAcquisitionSlave(musst_board,
                                           store_list = store_list)
    conversion = lambda data : (data / musst_clock)
    musst_master.add_external_channel(musst_device, 'time', rename='elapsed_time',
                                      conversion=conversion, dtype=numpy.float64)
    # MUSST: add musst in the acquisition chain
    chain.add(musst_master, musst_device)

    # P201: create a master  
    if "p201_eh2" in enabled_counters_name:

        
        p201_eh2 = setup_globals.p201_eh2
        p201 = CT2AcquisitionMaster(p201_eh2,
                                    npoints=nbpoints,
                                    acq_mode=AcqMode.ExtTrigMulti,
                                    acq_expo_time=exp_time)

        # musst is master and p201 its slave
        chain.add(musst_master, p201)
    
        enabled_counters_name.remove('p201_eh2')       
        p201_eh2 = IntegratingCounterAcquisitionSlave(*p201_eh2.counters, npoints=nbpoints,
                                                         count_time=exp_time)

        chain.add(p201, p201_eh2)

    lima_parameters = {'acq_trigger_mode':'EXTERNAL_TRIGGER_MULTI',
                           'acq_expo_time':exp_time,
                           'acq_nb_frames':nbpoints, 'save_flag':save}
    if save:
    # set saving parameters
        if zaptime_parameters.saving_format == 'edfgz':
            lima_parameters['saving_format']="EDFGZ"
            lima_parameters['saving_suffix']='.edf.gz'
            lima_parameters['saving_frame_per_file']=1
        elif zaptime_parameters.saving_format == 'hdf5bs':
            lima_parameters['saving_format']="HDF5BS"
            lima_parameters['saving_suffix']='.h5'
            lima_parameters['saving_frame_per_file']=nbpoints
        elif zaptime_parameters.saving_format == 'hdf5gz':
            lima_parameters['saving_format']="HDF5GZ"
            lima_parameters['saving_suffix']='.h5'
            lima_parameters['saving_frame_per_file']=nbpoints
        elif zaptime_parameters.saving_format == 'hdf5':
            lima_parameters['saving_format']="HDF5"
            lima_parameters['saving_suffix']='.h5'
            lima_parameters['saving_frame_per_file']=nbpoints
        else:
            lima_parameters['saving_format']="EDF"
            lima_parameters['saving_suffix']='.edf'            
            lima_parameters['saving_frame_per_file']=1
        save_msg = 'Saving [{0}{1}{2}]'.format(bcolor.BOLD,lima_parameters['saving_format'], bcolor.END)
    else:
        save_msg = 'Saving [{0}OFF{1}]'.format(bcolor.BOLD, bcolor.END)
        
    print('Exposure time {0} sec, Interval time {1} sec({2:.3f} fps), {3}'.format(exp_time, interval_time, 1/interval_time, save_msg))
            
    detectors_2d = list()
    for det in camera_latency.keys():
        det_acq = LimaAcquisitionMaster(det,**lima_parameters)
        det_acq.add_counter(det.image)

        chain.add(musst_master, det_acq)
        
        detectors_2d.append(det)

        roi_counters = det.counter_groups.roi_counters
        if roi_counters:
            det_counters = IntegratingCounterAcquisitionSlave(*roi_counters,
                                                             npoints=nbpoints,count_time=exp_time)
            chain.add(det_acq, det_counters)

    # add to the fast chain a top master timer for 'slow' counters like srcur, eurotherm ...
    scan_params={'npoints':0,
                 'count_time':min(.01,exp_time),
    }

    if enabled_counters_name:
        left_counters = [getattr(setup_globals,name) for name in enabled_counters_name]
        chain.append(scans.DEFAULT_CHAIN.get(scan_params, left_counters))
    
            
    #saving_parameters = setup_globals.SCAN_SAVING.get()
    
    scan_info = {'title': f'zaptime ({nbpoints}, {exp_time: g}, {latency_time: g}), interval time = {interval_time: g}',
                 'save': save}
    scan = Scan(chain,name='zaptime',
                scan_info=scan_info,
                save=save,
                data_watch_callback=CScanDisplay(detectors_2d,nbpoints) )
#                data_watch_callback=StepScanDataWatch() )
    
    t0 = time.time()
    scan.run()
    t = time.time()-t0
    print ("Zaptime took {0}{1}{2} (estimated {3}, delta {4})".format(
        bcolor.BOLD,
        datetime.timedelta(seconds=t),
        bcolor.END,
        datetime.timedelta(seconds=total_time),
        datetime.timedelta(seconds=t-total_time))
    )
    return scan


class CScanDisplay(object):
    def __init__(self,detectors, nb_points):
        self.nb_points = nb_points
        self.detectors = detectors
        self.detector_info = dict()
        self.display = ''
        
    def on_state(self,state):
        return False
    
    def on_scan_new(self,*args):
        self.display = ''
        
    def on_scan_end(self, info):
        pass
    
    def on_scan_data(self,data_events,nodes,info):
        display_list = list()
        if self.detectors:
            for det in self.detectors:
                curr_image = det.proxy.last_image_ready
                display_list.append(
                    '{0} {1}/{2}'.format(det.name,
                                         curr_image+1,
                                         self.nb_points)
                )

                self.display = ' '.join(display_list)
        else:
            if len(self.display) == 80 or len(self.display) == 0:
                self.display = '>'
            self.display = '=' + self.display
        if self.display:
            print (self.display,'\r', end='\r'),

            
class MultiplexerPreset(ChainPreset):
    def __init__(self, shutter_mode):
        self.shutter_mode = shutter_mode
        ChainPreset.__init__(self)
        
    multiplexer = setup_globals.multiplexer_eh2
    
    def prepare(self, acq_chain):
        pass

    def start(self, acq_chain):
        self.multiplexer.switch('ACQ_TRIG_EH2','ZAP_SHUTTER_DELAY')
        
        if self.shutter_mode == 'sequence':
            self.multiplexer.switch('SHUTTER_MODE', 'MANUAL')
            self.multiplexer.switch('SHUTTER_MANUAL', 'OPEN')
        else: 
            self.multiplexer.switch('SHUTTER_MODE', 'ZAP')              
    
    def stop(self, acq_chain):
        if self.shutter_mode == 'sequence':
            #print('Shutter closed')
            self.multiplexer.switch('SHUTTER_MANUAL', 'CLOSE')
            
        self.multiplexer.switch('SHUTTER_MODE', 'ALL')            
        self.multiplexer.switch('ACQ_TRIG_EH2','COUNT')
        
            

class bcolor:
   PURPLE = '\033[95m'
   CYAN = '\033[96m'
   DARKCYAN = '\033[36m'
   BLUE = '\033[94m'
   GREEN = '\033[92m'
   YELLOW = '\033[93m'
   RED = '\033[91m'
   BOLD = '\033[1m'
   UNDERLINE = '\033[4m'
   END = '\033[0m'
