from bliss.scanning.chain import ChainPreset, ChainIterationPreset
from bliss.common.scans.step_by_step import loopscan
from bliss import setup_globals, global_map
from bliss.common.logtools import *

def loopscan_manual(npoints, count_time, save=True, save_manual = True, lima=None):

    if lima is None:
        print ("Missing a lima detector")
        return

    scan_info={'title': f"loopscan_manual {npoints} {count_time}"}
    ls = loopscan(npoints, count_time, lima, save=save, run=False, scan_info=scan_info)

    last_image = npoints-1
    tscan_preset = ManualSavingChainPreset(lima, save_manual, last_image)
    ls.acq_chain.add_preset(tscan_preset)

    s = ls.run()
    return s



class ManualSavingChainPreset(ChainPreset):

    """
    A ScanPreset to manage PerkinElmer background correction 
    """
    class Iterator(ChainIterationPreset):
        def __init__(self, preset, iteration_nb):
            self.iteration = iteration_nb
            self.preset = preset
            
        def prepare(self):
            pass
        
        def start(self):
            # check for beam otherwise wait
            print('In Iterator::start()')
                
        def stop(self):
            if self.iteration != self.preset.last_image:
                while self.preset.lima.proxy.last_image_ready != self.iteration:
                    gevent.sleep(0.01)
                log_debug('tscan', f"save image #{self.iteration}")
                self.preset.lima.proxy.writeImage(-1)                        
                
    def get_iterator(self,acq_chain):
        iteration_nb = 0
        while True:
            yield ManualSavingChainPreset.Iterator(self, iteration_nb)
            iteration_nb += 1            

    def __init__(self, lima, save_manual, last_image):
        super().__init__()
        self.lima = lima
        self.save_manual = save_manual
        self.last_image = last_image
        
        global_map.register('tscan')
        
    def prepare(self, scan):
        pass

        
    def start(self, scan):
        # set lima in manual saving mode
        if self.save_manual and self.lima.proxy.saving_directory:
            self.lima.proxy.saving_mode = "MANUAL"
            self.lima.proxy.prepareAcq()
        else:
            self.save_manual = False

    def stop(self, scan):
        pass

    def before_stop(self, scan):
        # in background image are not saved, save the last image here before
        # bliss stop the camera
        if self.save_manual:
            log_debug('tscan', f"save lima {self.lima.name} last image #{self.last_image}")
            self.lima.proxy.writeImage(-1)  




